import os
os.environ["CUDA_DIVECES_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
import argparse
from itertools import chain
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from torch.autograd import Variable
from dataset import *
from model import *
import scipy
from progressbar import ETA, Bar, Percentage, ProgressBar
import torchvision.models as t_models
from logger import Logger
import resnet as rn

parser = argparse.ArgumentParser(description='PyTorch implementation of O-Net pretrain - segmentor part')
parser.add_argument('--cuda', type=str, default='true', help='Set cuda usage')
parser.add_argument('--epoch_size', type=int, default=10, help='Set epoch size')
parser.add_argument('--batch_size', type=int, default=1, help='Set batch size')
parser.add_argument('--arch', type=str, default="MySegnet2", choices=["SBK","BB","BK","Ms_BK","Ms_Mc_BK","MySegnet3","MySegnet2", "DeepUnet", "MySegnet", "Segnet"], help='Set architecture you wanna train: MySegnet2, DeepUnet, MySegnet, Segnet')

parser.add_argument('--learning_seg_rate', type=float, default=0.003, help='Set learning rate for segnetor optimizer')
parser.add_argument('--learning_seg_momentum', type=float, default=0.3, help='Set learning momentum for segnetor')
parser.add_argument('--learning_seg_decay', type=float, default=0.005, help='Set learning weight decay for segnetor')
parser.add_argument('--learning_seg_sc_type', type=str, default=None, choices=[None, "None", "step", "multi_step", "exponential"], help='Set learning scheme: None, step, multi step, exponential are supoorted')
parser.add_argument('--learning_seg_sc_gama', type=float, default=0.1, help='Set gama for segnetor learning scheme, only used in type step, multi step')
parser.add_argument('--learning_seg_sc_stepsize', type=int, default=1000, help='Set step for segnetor learning scheme, only used in step')
parser.add_argument('--learning_seg_sc_milestones', type=str, default="1000,1500", help='Set milestones for segnetor learning scheme, only used multi step')

parser.add_argument('--class_weight', type=str, default=None, help='class weight')
parser.add_argument('--result_path', type=str, default='./results/',
                    help='Set the path the result images will be saved.')
parser.add_argument('--model_path', type=str, default='./models/', help='Set the path for trained models')
parser.add_argument('--logs_path', type=str, default='./logs', help='Set the path for logs')
parser.add_argument('--image_size', type=int, default=256, help='Image size. 256 for every experiment in the paper')
parser.add_argument('--n_classes', type=int, default=6, help='classes numbers')

parser.add_argument('--data_pair', type=str, default=None, required=True,
                    help='data in pair to train')
parser.add_argument('--test_pair', type=str, default=None, required=True,
                    help='test data in pair to do test')

parser.add_argument('--segnet_path', type=str, default=None,
                    help='segnet weights model path to load')

parser.add_argument('--log_interval', type=int, default=100, help='Print loss values every log_interval iterations.')
parser.add_argument('--image_save_interval', type=int, default=1000,
                    help='Save test results every image_save_interval iterations.')
parser.add_argument('--model_save_interval', type=int, default=1000,
                    help='Save models every model_save_interval iterations.')


def as_np(data):
    return data.cpu().data.numpy()


def to_np(x):
    return x.data.cpu().numpy()


def cross_entropy2d(input, target, weight=None, size_average=True):
    n, c, h, w = input.size()
    input_1d = input.transpose(1, 2).transpose(2, 3).contiguous().view(-1, c)
    n, c, h, w = target.size()
    target_1d = target.transpose(1, 2).transpose(2, 3).contiguous().view(-1)

    return F.cross_entropy(input_1d, target_1d, weight, size_average )


def str_to_list(str):
    if str is None:
        return None

    int_str_list = str.split(",")
    int_list = []
    for int_str in int_str_list:
        int_list.append(int(int_str))

    return int_list


def str_to_list_float(str):
    if str is None:
        return None

    float_str_list = str.split(",")
    float_list = []
    for float_str in float_str_list:
        float_list.append(float(float_str))

    return float_list


def get_accuracy(produced, true):
    equal = torch.eq(produced.data, true.data)

    true_number = torch.sum(equal)
    total_number = torch.numel(equal)

    return 1. * true_number / total_number


def get_accuracy_perclass(produced, true, n_class):
    produced_np = as_np(produced)
    true_np = as_np(true)

    accuracy_list = []

    for class_i in range(n_class):
        class_true = true_np.copy()
        class_true[class_true != class_i] = -1

        equal = np.equal(produced_np, class_true)
        true_number = np.sum(equal)

        total = np.equal(class_true, class_i)
        total_number = np.sum(total)

        if total_number == 0:
            accuracy = 1
        else:
            accuracy = 1. * true_number / total_number

        accuracy_list.append(accuracy)

    return accuracy_list


def get_ac_IoU_F1(produced, true, n_class):
    produced_np = as_np(produced)
    true_np = as_np(true)

    accuracy_list = []
    IoU_list = []
    F1_list = []

    for class_i in range(n_class):
        class_true = true_np.copy()
        class_true[class_true != class_i] = -1
        class_true[class_true == class_i] = 1
        class_true[class_true == -1] = 0

        class_produced = produced_np.copy()
        class_produced[class_produced != class_i] = -1
        class_produced[class_produced == class_i] = 1

        sumup_array = class_true + class_produced

        #accuracy
        true_number = np.sum(sumup_array == -1) + np.sum(sumup_array == 2)
        total_number = sumup_array.size

        if total_number == 0:
            accuracy = 1
        else:
            accuracy = 1. * true_number / total_number

        accuracy_list.append(accuracy)

        # IoU
        intersection = np.sum(sumup_array == 2)
        union = np.sum(sumup_array >= 0)

        if union == 0:
            IoU = 1
        else:
            IoU = 1. * intersection / union

        IoU_list.append(IoU)

        # F1
        TP = np.sum(sumup_array == 2)
        FP = np.sum(sumup_array == 1)
        FN = np.sum(sumup_array == 0)
        TN = np.sum(sumup_array == -1)

        if 2 * TP + FP + FN == 0:
            F1 = 1
        else:
            F1 = 2. * TP / (2 * TP + FP + FN)

        F1_list.append(F1)

    return accuracy_list, IoU_list, F1_list


class EmptyLrSchema:
    def __init__(self, lr):
        self.lr = lr

    def step(self):
        pass

    def get_lr(self):
        return [self.lr]


def test_old(result_path, epoch, iters, test_pair, batch_size, cuda, segnet):
    subdir_path = os.path.join(result_path, str(iters))

    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)

    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size

    accuracy_list = []
    per_accuracy_array = []
    IoU_array = []
    F1_array = []

    for test_batch_iter in range(n_batchs_test):
        # read test image
        # test_image = read_images(
        #     test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
        #     args.image_size)
        # test_image_128 = read_images(
        #     test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
        #     args.image_size/2)
        # test_image_64 = read_images(
        #     test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
        #     args.image_size/4)

        test_image,test_image_128,test_image_64 = read_images_Ms(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)

        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)

        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_image_128 = Variable(torch.FloatTensor(test_image_128), volatile=True)
        test_image_64 = Variable(torch.FloatTensor(test_image_64), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)

        if cuda:
            test_image = test_image.cuda()
            test_image_128 = test_image_128.cuda()
            test_image_64 = test_image_64.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()

        label_prob = segnet(test_image,test_image_128,test_image_64)
        max_val, label_produce = torch.max(label_prob, 1)
        label_produce = label_produce[:, np.newaxis, :, :]

        accuracy_list.append(get_accuracy(label_produce, test_label_long))
        per_accuracy_array_item, IoU_array_item, F1_array_item = get_ac_IoU_F1(label_produce, test_label_long, args.n_classes)
        per_accuracy_array.append(per_accuracy_array_item)
        IoU_array.append(IoU_array_item)
        F1_array.append(F1_array_item)

        n_testset = test_image.size()[0]

        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            label_produce_Val = label_colorize(
                label_produce[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))

            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.seg.jpg',
                              label_produce_Val.astype(np.uint8)[:, :, ::-1])

    accuracy = np.mean(np.array(accuracy_list))
    per_accuracy = np.mean(np.array(per_accuracy_array), 0)
    IoU = np.mean(np.array(IoU_array), 0)
    IoU_mean = np.mean(IoU)
    F1 = np.mean(np.array(F1_array), 0)
    F1_mean = np.mean(F1)

    print "Test----------------------------------------------"
    print "\tepoch :", epoch
    print "iteration:", iters
    print "Accuracy:", accuracy
    for per_class in range(per_accuracy.shape[0]):
        print "\tClass", per_class, "accuracy:\t", per_accuracy[per_class]
    print "IoU:", IoU_mean
    for per_class in range(IoU.shape[0]):
        print "\tClass", per_class, "IoU:\t", IoU[per_class]
    print "F1:", F1_mean
    for per_class in range(F1.shape[0]):
        print "\tClass", per_class, "F1:\t", F1[per_class]
    print " "

def test(result_path, epoch, iters, test_pair, batch_size, cuda, segnet):
    subdir_path = os.path.join(result_path, str(iters))
    batch_size = 1
    segnet =segnet.eval()
    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)
    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size
    accuracy_list = []
    per_accuracy_array = []
    IoU_array = []
    F1_array = []
    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image,test_image_128,test_image_64 = read_images_Ms(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)
        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_image_128 = Variable(torch.FloatTensor(test_image_128), volatile=True)
        test_image_64 = Variable(torch.FloatTensor(test_image_64), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)
        if cuda:
            test_image = test_image.cuda()
            test_image_128 = test_image_128.cuda()
            test_image_64 = test_image_64.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()
        label_prob ,tnp = segnet(test_image,test_image_128,test_image_64)
        max_val, label_produce = torch.max(label_prob, 1)
        label_produce = label_produce[:, np.newaxis, :, :]
        accuracy_list.append(get_accuracy(label_produce, test_label_long))
        per_accuracy_array_item, IoU_array_item, F1_array_item = get_ac_IoU_F1(label_produce, test_label_long, args.n_classes)
        per_accuracy_array.append(per_accuracy_array_item)
        IoU_array.append(IoU_array_item)
        F1_array.append(F1_array_item)
        n_testset = test_image.size()[0]
        
        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            #image_real_Val=image_real_Val[:,:,0::2]
            label_produce_Val = label_colorize(
                label_produce[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            
            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.seg.jpg',
                              label_produce_Val.astype(np.uint8)[:, :, ::-1])
        
            
    accuracy = np.mean(np.array(accuracy_list))
    per_accuracy = np.mean(np.array(per_accuracy_array), 0)
    IoU = np.mean(np.array(IoU_array), 0)
    IoU_mean = np.mean(IoU)
    F1 = np.mean(np.array(F1_array), 0)
    F1_mean = np.mean(F1)
    print "Test----------------------------------------------"
    print "\tepoch :", epoch
    print "iteration:", iters
    print "Accuracy:", accuracy
    for per_class in range(per_accuracy.shape[0]):
        print "\tClass", per_class, "accuracy:\t", per_accuracy[per_class]
    print "IoU:", IoU_mean
    for per_class in range(IoU.shape[0]):
        print "\tClass", per_class, "IoU:\t", IoU[per_class]
    print "F1:", F1_mean
    for per_class in range(F1.shape[0]):
        print "\tClass", per_class, "F1:\t", F1[per_class]
    print " "
    segnet =segnet.train()
    

def test_MS(result_path, epoch, iters, test_pair, batch_size, cuda, segnet):
    subdir_path = os.path.join(result_path, str(iters))
    segnet.eval()
    batch_size=1
    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)

    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size

    accuracy_list = []
    per_accuracy_array = []
    IoU_array = []
    F1_array = []

    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)

        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)

        if cuda:
            test_image = test_image.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()

        label_prob = segnet(test_image)
        max_val, label_produce = torch.max(label_prob, 1)
        label_produce = label_produce[:, np.newaxis, :, :]

        accuracy_list.append(get_accuracy(label_produce, test_label_long))
        per_accuracy_array_item, IoU_array_item, F1_array_item = get_ac_IoU_F1(label_produce, test_label_long, args.n_classes)
        per_accuracy_array.append(per_accuracy_array_item)
        IoU_array.append(IoU_array_item)
        F1_array.append(F1_array_item)

        n_testset = test_image.size()[0]

        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            label_produce_Val = label_colorize(
                label_produce[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))

            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.seg.jpg',
                              label_produce_Val.astype(np.uint8)[:, :, ::-1])

    accuracy = np.mean(np.array(accuracy_list))
    per_accuracy = np.mean(np.array(per_accuracy_array), 0)
    IoU = np.mean(np.array(IoU_array), 0)
    IoU_mean = np.mean(IoU)
    F1 = np.mean(np.array(F1_array), 0)
    F1_mean = np.mean(F1)

    print "Test----------------------------------------------"
    print "\tepoch :", epoch
    print "iteration:", iters
    print "Accuracy:", accuracy
    for per_class in range(per_accuracy.shape[0]):
        print "\tClass", per_class, "accuracy:\t", per_accuracy[per_class]
    print "IoU:", IoU_mean
    for per_class in range(IoU.shape[0]):
        print "\tClass", per_class, "IoU:\t", IoU[per_class]
    print "F1:", F1_mean
    for per_class in range(F1.shape[0]):
        print "\tClass", per_class, "F1:\t", F1[per_class]
    print " "


def save_model(segnet, epoch, iters, model_path):
    torch.save(segnet.state_dict(),
               os.path.join(model_path, str(iters) + '-segnet' + '.pkl'))
    print "save models at ", "epoch ", epoch, "iteration ", iters
    print "store in ", model_path
    print " "


def learning_rate_producer(optimizer, method, step_size=0, milestones=None, gamma=0.1, last_epoch=-1):
    if milestones is None:
        milestones = []
    if method is None or method == "None":
        return EmptyLrSchema(optimizer.param_groups[0]['lr'])
    elif method == "step":
        return lr_scheduler.StepLR(optimizer, step_size, gamma, last_epoch)
    elif method == 'multi_step':
        return lr_scheduler.MultiStepLR(optimizer, milestones, gamma, last_epoch)
    elif method == "exponential":
        return lr_scheduler.ExponentialLR(optimizer, gamma, last_epoch)


def log_print(epoch, iters, seg_loss, lr_manager_seg):

    print "epoch:", epoch
    print "iteration:", iters
    print "\tSEG Loss:", as_np(seg_loss.mean())
    print "\tSegmentor Learning Rate:", lr_manager_seg.get_lr()[0]
    print " "


def log_tensorboard(logger, seg_loss, iters, segnet):

    info = {
        'seg_loss': as_np(seg_loss.mean())[0].item(),
    }

    for tag, value in info.items():
        logger.scalar_summary(tag, value, iters)

    for tag, value in segnet.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)

def main():
    global args
    args = parser.parse_args()
    print "hello world !"
    cuda = args.cuda
    if cuda == 'true':
        cuda = True
    else:
        cuda = False

    batch_size = args.batch_size

    epoch_size = args.epoch_size

    if args.class_weight is not None:
        class_weight = np.array(str_to_list_float(args.class_weight))
        class_weight = Variable(torch.FloatTensor(class_weight))
        if cuda:
            class_weight = class_weight.cuda()
    else:
        class_weight = None

    logger = Logger(args.logs_path)

    result_path = args.result_path
    model_path = args.model_path

    data_pair, test_pair = get_data_pretrain(args.data_pair, args.test_pair)

    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_path):
        os.makedirs(model_path)

    if args.arch == "Segnet":
        segnet = Segnet()
    elif args.arch == "MySegnet":
        segnet = MySegnet()
    elif args.arch == "DeepUnet":
        segnet = DeepUnet()
    elif args.arch == "MySegnet2":
        segnet = MySegnet2()
    elif args.arch == "BB":
        segnet = rn.ResSegNet(rn.BasicBlock, [2, 2, 2, 2])
    elif args.arch == "BK":
        segnet = rn.ResSegNet_BK(rn.Bottleneck, [2, 2, 2, 2])
    elif args.arch == "SBK":
        segnet = rn.ResSegNet_BK(rn.S_Bottleneck, [2, 2, 2, 2])
    elif args.arch == "Ms_BK":
        segnet = rn.ResSegNet_Mix(rn.S_Bottleneck, [2, 2, 2, 2])
    else:
        print "Unknown architecture, use Mysegnet2 default"
        segnet = MySegnet2()

    # load weights
    
    if args.segnet_path is not None:
        if os.path.isfile(args.segnet_path):
            print "loading segnet weights from:", args.segnet_path
            segnet.load_state_dict(torch.load(args.segnet_path))
        else:
            print "Warning: File not exit:", args.segnet_path

    if cuda:
        segnet = segnet.cuda()
    
    data_size_pair = data_pair.shape[0]
    n_batches_pair = data_size_pair // batch_size

    seg_criterion = cross_entropy2d
    
    seg_params = segnet.parameters()
    # segnet.FE_parameters()

    optim_seg = optim.SGD(seg_params, lr=args.learning_seg_rate, momentum=args.learning_seg_momentum, weight_decay=args.learning_seg_decay)

    lr_manager_seg = learning_rate_producer(optimizer=optim_seg,
                                            method=args.learning_seg_sc_type,
                                            step_size=args.learning_seg_sc_stepsize,
                                            milestones=str_to_list(args.learning_seg_sc_milestones),
                                            gamma=args.learning_seg_sc_gama)
    iters = 0

    for epoch in range(epoch_size):
        data_pair = shuffle_data(data_pair)

        for i in range(n_batches_pair):

            # pbar.update(i)

            segnet.zero_grad()

            image_real_path = data_pair[i * batch_size: (i + 1) * batch_size, 0]
            label_true_path = data_pair[i * batch_size: (i + 1) * batch_size, 1]

            image_real, image_real_128, image_real_64 = read_images_Ms(image_real_path, args.image_size)
            # image_real = read_images(image_real_path, args.image_size)
            # #print "image_real",image_real.shape
            # image_real_128 = read_images(image_real_path, args.image_size/2)
            # #print "image_real",image_real.shape
            # image_real_64 = read_images(image_real_path, args.image_size/4)
            # #print "image_real",image_real.shape
            label_true = read_labels_to_classes_chanels(label_true_path, args.n_classes, args.image_size)
            #print "label_true",label_true.shape
            label_true_long = read_images(label_true_path, args.image_size, False, True, True)

            image_real = Variable(torch.FloatTensor(image_real))
            image_real_128 = Variable(torch.FloatTensor(image_real_128))
            image_real_64 = Variable(torch.FloatTensor(image_real_64))
            label_true_long = Variable(torch.LongTensor(label_true_long))
            label_true = Variable(torch.FloatTensor(label_true))

            if cuda:
                image_real = image_real.cuda()
                image_real_128 = image_real_128.cuda()
                image_real_64 = image_real_64.cuda()
                label_true = label_true.cuda()
                label_true_long = label_true_long.cuda()

            label_prob ,tmp = segnet(image_real ,image_real_128,image_real_64)
            #print "label_prob",label_prob.size()
            # compute loss
            seg_loss = seg_criterion(label_prob, label_true_long, class_weight)
            
            seg_loss.backward()
            optim_seg.step()

            lr_manager_seg.step()

            if iters % args.log_interval == 0:
                log_print(epoch, iters, seg_loss, lr_manager_seg)

                log_tensorboard(logger, seg_loss, iters, segnet)

            if iters % args.image_save_interval == 0:
                # free gpu
                del image_real
                del label_true
                del label_true_long

                test(result_path,epoch, iters, test_pair, batch_size, cuda, segnet)

            if iters % args.model_save_interval == 0:
                save_model(segnet, epoch, iters, model_path)

            iters += 1


if __name__ == "__main__":
    main()
