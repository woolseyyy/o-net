import os
gpu_id=2
os.environ["CUDA_DIVECES_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="2"
import argparse
from itertools import chain
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from torch.autograd import Variable
from dataset import *
from model import *
import resnet as rn
import networks as nt
import scipy
from progressbar import ETA, Bar, Percentage, ProgressBar
import torchvision.models as t_models
from logger import Logger

parser = argparse.ArgumentParser(description='PyTorch implementation of O-Net - pretrain generator part')
parser.add_argument('--cuda', type=str, default='true', help='Set cuda usage')
parser.add_argument('--epoch_size', type=int, default=50, help='Set epoch size')
parser.add_argument('--batch_size', type=int, default=1, help='Set batch size')

parser.add_argument('--learning_gan_rate', type=float, default=0.0002, help='Set learning rate for gan optimizer')
parser.add_argument('--learning_gan_decay', type=float, default=0.00001, help='Set learning weight decay for gan')
parser.add_argument('--learning_gan_sc_type', type=str, default=None, choices=[None, "None", "step", "multi_step", "exponential"], help='Set learning scheme: None, step, multi step, exponential are supoorted')
parser.add_argument('--learning_gan_sc_gama', type=float, default=0.1, help='Set gama for gan learning scheme, only used in type step, multi step')
parser.add_argument('--learning_gan_sc_stepsize', type=int, default=1000, help='Set step for gan learning scheme, only used in step')
parser.add_argument('--learning_gan_sc_milestones', type=str, default="1000,1500", help='Set milestones for gan learning scheme, only used multi step')

parser.add_argument('--class_weight', type=str, default=None, help='class weight')
parser.add_argument('--result_path', type=str, default='./results/',
                    help='Set the path the result images will be saved.')
parser.add_argument('--model_path', type=str, default='./models/', help='Set the path for trained models')
parser.add_argument('--logs_path', type=str, default='./logs', help='Set the path for logs')
parser.add_argument('--image_size', type=int, default=256, help='Image size. 256 for every experiment in the paper')
parser.add_argument('--n_classes', type=int, default=6, help='classes numbers')

parser.add_argument('--gan_curriculum', type=int, default=10000,
                    help='Strong GAN loss for certain period at the beginning')
parser.add_argument('--starting_rate', type=float, default=0.01,
                    help='Set the lambda weight between GAN loss and Recon loss during curriculum period at the beginning. We used the 0.01 weight.')
parser.add_argument('--default_rate', type=float, default=0.5,
                    help='Set the lambda weight between GAN loss and Recon loss after curriculum period. We used the 0.5 weight.')

parser.add_argument('--data_pair', type=str, default=None, required=True,
                    help='data in pair to train')
parser.add_argument('--test_pair', type=str, default=None, required=True,
                    help='test data in pair to do test')

parser.add_argument('--generator_path', type=str, default=None,
                    help='generator weights model path to load')
parser.add_argument('--discriminator_path', type=str, default=None,
                    help='discriminator weights model path to load')

parser.add_argument('--update_interval', type=int, default=3, help='')
parser.add_argument('--log_interval', type=int, default=100, help='Print loss values every log_interval iterations.')
parser.add_argument('--image_save_interval', type=int, default=1000,
                    help='Save test results every image_save_interval iterations.')
parser.add_argument('--model_save_interval', type=int, default=1000,
                    help='Save models every model_save_interval iterations.')


def as_np(data):
    return data.cpu().data.numpy()


def to_np(x):
    return x.data.cpu().numpy()


def get_fm_loss(real_feats, fake_feats, criterion):
    losses = 0
    for real_feat, fake_feat in zip(real_feats, fake_feats):
        l2 = (real_feat.mean(0) - fake_feat.mean(0)) * (real_feat.mean(0) - fake_feat.mean(0))
        loss = criterion(l2, Variable(torch.ones(l2.size())).cuda())
        losses += loss

    return losses


def get_gan_loss_old(dis_real, dis_fake, criterion, cuda):
    EPS=0.0001
    
    # dis_real=dis_real.view(dis_real.size()[0],30*30)
    # dis_fake=dis_fake.view(dis_fake.size()[0],30*30)
    labels_dis_real = Variable(torch.ones([dis_real.size()[0],dis_real.size()[1],dis_real.size()[2],dis_real.size()[3]]))
    
    labels_dis_fake = Variable(torch.zeros([dis_fake.size()[0],dis_fake.size()[1],dis_fake.size()[2],dis_fake.size()[3]]))
    
    labels_gen = Variable(torch.ones([dis_real.size()[0],dis_real.size()[1],dis_real.size()[2],dis_real.size()[3]]))
    


    # if cuda:
    #     labels_dis_real = labels_dis_real.cuda()
    #     labels_dis_fake = labels_dis_fake.cuda()
    #     labels_gen = labels_gen.cuda()

    # dis_loss = criterion(dis_real, labels_dis_real) * 0.5 + criterion(dis_fake, labels_dis_fake) * 0.5
    # gen_loss = criterion(dis_fake, labels_gen)

    dis_loss = torch.mean(-(torch.log(dis_real + EPS) + torch.log(1 - dis_fake + EPS)))
    gen_loss = torch.mean(-torch.log(dis_fake + EPS))
    

    return dis_loss, gen_loss

def get_gan_loss(dis_real, dis_fake, criterion, cuda):
    # labels_dis_real = Variable(torch.ones([dis_real.size()[0], 1]))
    # labels_dis_fake = Variable(torch.zeros([dis_fake.size()[0], 1]))
    # labels_gen = Variable(torch.ones([dis_fake.size()[0], 1]))

    # if cuda:
    #     labels_dis_real = labels_dis_real.cuda()
    #     labels_dis_fake = labels_dis_fake.cuda()
    #     labels_gen = labels_gen.cuda()

    # dis_loss = criterion(dis_real, labels_dis_real) * 0.5 + criterion(dis_fake, labels_dis_fake) * 0.5
    # gen_loss = criterion(dis_fake, labels_gen)
    
    # EPS=0.0001
    
    # labels_dis_real = Variable(torch.ones([dis_real.size()[0],dis_real.size()[1],dis_real.size()[2],dis_real.size()[3]]))
    # labels_dis_fake = Variable(torch.zeros([dis_fake.size()[0],dis_fake.size()[1],dis_fake.size()[2],dis_fake.size()[3]]))
    # labels_gen = Variable(torch.ones([dis_real.size()[0],dis_real.size()[1],dis_real.size()[2],dis_real.size()[3]]))
    
    # dis_loss = torch.mean(-(torch.log(dis_real + EPS) + torch.log(1 - dis_fake + EPS)))
    # gen_loss = torch.mean(-torch.log(dis_fake + EPS))
    
    criterionGAN = nt.GANLoss()

    dis_loss = ( criterionGAN(dis_real, True) + criterionGAN(dis_fake, False) ) * 0.5
    gen_loss = criterionGAN(dis_fake, True)

    return dis_loss, gen_loss

def str_to_list(str):
    if str is None:
        return None

    int_str_list = str.split(",")
    int_list = []
    for int_str in int_str_list:
        int_list.append(int(int_str))

    return int_list


def str_to_list_float(str):
    if str is None:
        return None

    float_str_list = str.split(",")
    float_list = []
    for float_str in float_str_list:
        float_list.append(float(float_str))

    return float_list


def get_accuracy(produced, true):
    equal = torch.eq(produced.data, true.data)

    true_number = torch.sum(equal)
    total_number = torch.numel(equal)

    return 1. * true_number / total_number


class EmptyLrSchema:
    def __init__(self, lr):
        self.lr = lr

    def step(self):
        pass

    def get_lr(self):
        return [self.lr]


def test_old(result_path, epoch, iters, test_pair, batch_size, cuda, generator):
    subdir_path = os.path.join(result_path, str(iters))

    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)

    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size

    accuracy_list = []

    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)

        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)

        if cuda:
            test_image = test_image.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()

        image_fake = generator(test_label)

        n_testset = test_image.size()[0]

        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            image_fake_Val = image_fake[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.

            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.gen.jpg', image_fake_Val.astype(np.uint8)[:, :, ::-1])

    print "Test----------------------------------------------"
    print "epoch :", epoch
    print "iteration:", iters
    print "Generated images have been store in: ", subdir_path
    print " "


def test__old(result_path, epoch, iters, test_pair, batch_size, cuda, segnet):
    subdir_path = os.path.join(result_path, str(iters))
    batch_size = 8
    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)
    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size
    accuracy_list = []
    per_accuracy_array = []
    IoU_array = []
    F1_array = []
    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image,test_image_128 ,test_image_64 = read_images_Ms(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        # test_image_128 = read_images_Mc(
        #     test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
        #     args.image_size/2)
        # test_image_64 = read_images_Mc(
        #     test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
        #     args.image_size/4)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)
        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_image_128 = Variable(torch.FloatTensor(test_image_128), volatile=True)
        test_image_64 = Variable(torch.FloatTensor(test_image_64), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)
        if cuda:
            test_image = test_image.cuda()
            test_image_128 = test_image_128.cuda()
            test_image_64 = test_image_64.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()
        label_prob = segnet(test_image ,test_image_128,test_image_64)
        max_val, label_produce = torch.max(label_prob, 1)
        label_produce = label_produce[:, np.newaxis, :, :]
        
        accuracy_list.append(get_accuracy(label_produce, test_label_long))
        per_accuracy_array_item, IoU_array_item, F1_array_item = get_ac_IoU_F1(label_produce, test_label_long, args.n_classes)
        per_accuracy_array.append(per_accuracy_array_item)
        IoU_array.append(IoU_array_item)
        F1_array.append(F1_array_item)
        n_testset = test_image.size()[0]
        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            
            label_produce_Val = label_colorize(
                label_produce[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            
            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.seg.jpg',
                              label_produce_Val.astype(np.uint8)[:, :, ::-1])
            
    accuracy = np.mean(np.array(accuracy_list))
    per_accuracy = np.mean(np.array(per_accuracy_array), 0)
    IoU = np.mean(np.array(IoU_array), 0)
    IoU_mean = np.mean(IoU)
    F1 = np.mean(np.array(F1_array), 0)
    F1_mean = np.mean(F1)
    print "Test----------------------------------------------"
    print "\tepoch :", epoch
    print "iteration:", iters
    print "Accuracy:", accuracy
    for per_class in range(per_accuracy.shape[0]):
        print "\tClass", per_class, "accuracy:\t", per_accuracy[per_class]
    print "IoU:", IoU_mean
    for per_class in range(IoU.shape[0]):
        print "\tClass", per_class, "IoU:\t", IoU[per_class]
    print "F1:", F1_mean
    for per_class in range(F1.shape[0]):
        print "\tClass", per_class, "F1:\t", F1[per_class]
    print " "
def test(result_path, epoch, iters, test_pair, batch_size, cuda, generator):
    subdir_path = os.path.join(result_path, str(iters))

    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)

    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size

    accuracy_list = []

    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)

        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)

        if cuda:
            test_image = test_image.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()

        image_fake,temp = generator(test_label ,None)

        n_testset = test_image.size()[0]

        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            image_fake_Val = image_fake[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.

            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.gen.jpg', image_fake_Val.astype(np.uint8)[:, :, ::-1])

    print "Test----------------------------------------------"
    print "epoch :", epoch
    print "iteration:", iters
    print "Generated images have been store in: ", subdir_path
    print " "


def save_model(generator, discriminator, epoch, iters, model_path):
    torch.save(generator.state_dict(),
               os.path.join(model_path, str(iters) + '-generator' + '.pkl'))
    torch.save(discriminator.state_dict(),
               os.path.join(model_path, str(iters) + '-discriminator' + '.pkl'))
    print "save models at ", "epoch ", epoch, ", iteration ", iters
    print "store in ", model_path
    print " "


def learning_rate_producer(optimizer, method, step_size=0, milestones=None, gamma=0.1, last_epoch=-1):
    if milestones is None:
        milestones = []
    if method is None or method == "None":
        return EmptyLrSchema(optimizer.param_groups[0]['lr'])
    elif method == "step":
        return lr_scheduler.StepLR(optimizer, step_size, gamma, last_epoch)
    elif method == 'multi_step':
        return lr_scheduler.MultiStepLR(optimizer, milestones, gamma, last_epoch)
    elif method == "exponential":
        return lr_scheduler.ExponentialLR(optimizer, gamma, last_epoch)


def log_print(epoch, iters, gen_loss_total, gen_loss, recon_loss, dis_loss, lr_manager_gen, lr_manager_dis):
    print "epoch:", epoch
    print "iteration:", iters
    print "\tGEN Total Loss:", as_np(gen_loss_total.mean())
    print "\tGEN Loss:", as_np(gen_loss.mean())
    print "\tRECON Loss:", as_np(recon_loss.mean())
    print "\tDIS Loss:", as_np(dis_loss.mean())
    print "\tGenerator Learning Rate:", lr_manager_gen.get_lr()[0]
    print "\tDiscriminator Learning Rate:", lr_manager_dis.get_lr()[0]
    print " "


def log_tensorboard(logger, gen_loss_total, dis_loss, iters, generator, discriminator):

    info = {
        'gen_loss_total': as_np(gen_loss_total.mean())[0].item(),
        'dis_loss': as_np(dis_loss.mean())[0].item()
    }

    for tag, value in info.items():
        logger.scalar_summary(tag, value, iters)

    for tag, value in generator.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)

    for tag, value in discriminator.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)


def main():
    global args
    args = parser.parse_args()

    cuda = args.cuda
    if cuda == 'true':
        cuda = True
    else:
        cuda = False

    batch_size = args.batch_size

    epoch_size = args.epoch_size

    if args.class_weight is not None:
        class_weight = np.array(str_to_list_float(args.class_weight))
        class_weight = Variable(torch.FloatTensor(class_weight))
        if cuda:
            class_weight = class_weight.cuda()
    else:
        class_weight = None

    logger = Logger(args.logs_path)

    result_path = args.result_path
    model_path = args.model_path

    data_pair, test_pair = get_data_pretrain(args.data_pair, args.test_pair)

    # test_image = read_images(test_pair[:, 0], args.image_size)
    # test_label = read_images(test_pair[:, 1], args.image_size, False, True)
    #
    # test_image = Variable(torch.FloatTensor(test_image), volatile=True)
    # test_label = Variable(torch.FloatTensor(test_label), volatile=True)


    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_path):
        os.makedirs(model_path)

    # generator = Generator(args.n_classes)
    # discriminator = Discriminator()
    generator= nt.ResnetGenerator(args.n_classes, 3, 64, use_dropout=False,n_blocks=6, gpu_ids=[gpu_id])#rn.Gen_ResSegNet(rn.Bottleneck, [2, 2, 2, 2], args.n_classes)#
    discriminator = nt.NLayerDiscriminator(3+args.n_classes, 64, n_layers=3, gpu_ids=[gpu_id])
    # load weights
    if args.generator_path is not None:
        if os.path.isfile(args.generator_path):
            print "loading generator weights from:", args.generator_path
            generator.load_state_dict(torch.load(args.generator_path))
        else:
            print "Warning: File not exit:", args.generator_path

    if args.discriminator_path is not None:
        if os.path.isfile(args.discriminator_path):
            print "loading discriminator weights from:", args.discriminator_path
            discriminator.load_state_dict(torch.load(args.discriminator_path))
        else:
            print "Warning: File not exit:", args.discriminator_path

    if cuda:
        generator = generator.cuda()
        discriminator = discriminator.cuda()

    data_size_pair = data_pair.shape[0]
    n_batches_pair = data_size_pair // batch_size

    # recon_criterion = nn.MSELoss()
    recon_criterion = nn.L1Loss()
    gan_criterion = nn.BCELoss()
    feat_criterion = nn.HingeEmbeddingLoss()

    gen_params = generator.parameters()
    # generator.R_parameters()

    dis_params = discriminator.parameters()

    optim_gen = optim.Adam(gen_params, lr=args.learning_gan_rate, betas=(0.5, 0.999), weight_decay=args.learning_gan_decay)
    optim_dis = optim.Adam(dis_params, lr=args.learning_gan_rate, betas=(0.5, 0.999), weight_decay=args.learning_gan_decay)

    lr_manager_gen = learning_rate_producer(optimizer=optim_gen,
                                            method=args.learning_gan_sc_type,
                                            step_size=args.learning_gan_sc_stepsize,
                                            milestones=str_to_list(args.learning_gan_sc_milestones),
                                            gamma=args.learning_gan_sc_gama)
    lr_manager_dis = learning_rate_producer(optimizer=optim_dis,
                                            method=args.learning_gan_sc_type,
                                            step_size=args.learning_gan_sc_stepsize,
                                            milestones=str_to_list(args.learning_gan_sc_milestones),
                                            gamma=args.learning_gan_sc_gama)

    iters = 0
    for epoch in range(epoch_size):
        data_pair = shuffle_data(data_pair)

        for i in range(n_batches_pair):

            generator.zero_grad()
            discriminator.zero_grad()

            image_real_path = data_pair[i * batch_size: (i + 1) * batch_size, 0]
            label_true_path = data_pair[i * batch_size: (i + 1) * batch_size, 1]

            image_real = read_images(image_real_path, args.image_size)
            label_true = read_labels_to_classes_chanels(label_true_path, args.n_classes, args.image_size)
            label_true_long = read_images(label_true_path, args.image_size, False, True, True)

            image_real = Variable(torch.FloatTensor(image_real))
            label_true_long = Variable(torch.LongTensor(label_true_long))
            label_true = Variable(torch.FloatTensor(label_true))
            # print image_real.size()
            # print label_true.size()
            # s


            if cuda:
                image_real = image_real.cuda()
                label_true = label_true.cuda()
                label_true_long = label_true_long.cuda()

            image_fake, temp = generator(label_true )
            # print "image_fake",image_fake.size()
            # print "image_real",image_real.size()

            # compute loss
            recon_loss = recon_criterion(image_fake, image_real)

            real_pair = torch.cat((image_real, label_true), 1)
            fake_pair = torch.cat((image_fake, label_true), 1)

            dis_real = discriminator(real_pair)
            dis_fake = discriminator(fake_pair)

            # print "dis_real",dis_real.size()

            # todo don't understand fm_loss
            dis_loss, gen_loss = get_gan_loss(dis_real, dis_fake, gan_criterion, cuda)
            
            # fm_loss = get_fm_loss(feats_real, feats_fake, feat_criterion)

            # todo don't know if it is best way
            if iters < args.gan_curriculum:
                rate = args.starting_rate
            else:
                rate = args.default_rate

            # gen_loss_total = (gen_loss * 0.1 + fm_loss * 0.9) * (1. - rate) + recon_loss * rate
            gen_loss_total =  (gen_loss )+ recon_loss * 50

            # update parameters
            if iters % args.update_interval == 0:
                dis_loss.backward()
                optim_dis.step()
            else:
                gen_loss_total.backward()
                optim_gen.step()

            lr_manager_gen.step()
            lr_manager_dis.step()

            if iters % args.log_interval == 0:
                log_print(epoch, iters,
                          gen_loss_total, gen_loss, recon_loss, dis_loss,
                          lr_manager_gen, lr_manager_dis)

                log_tensorboard(logger, gen_loss_total, dis_loss, iters, generator, discriminator)

            if iters % args.image_save_interval == 0:
                # free gpu
                del image_real
                del label_true
                del label_true_long

                test(result_path, epoch, iters, test_pair, batch_size, cuda, generator)

            if iters % args.model_save_interval == 0:
                save_model(generator, discriminator, epoch, iters, model_path)

            iters += 1


if __name__ == "__main__":
    main()
