import os
import cv2
import numpy as np
import pandas as pd
from scipy.misc import imresize
import scipy.io


def get_data(data_pair_path, data_unpair_path, test_pair_path):
    data_pair = parse_pair(data_pair_path)
    data_unpair = parse_unpair(data_unpair_path)
    test_pair = parse_pair(test_pair_path)

    return np.array(data_pair), np.array(data_unpair), np.array(test_pair)


def get_data_pretrain(data_pair_path, test_pair_path):
    data_pair = parse_pair(data_pair_path)
    test_pair = parse_pair(test_pair_path)

    return np.array(data_pair), np.array(test_pair)


def parse_unpair(path):
    with open(path) as f:
        lines = f.readlines()
        result = []
        for line in lines:
            result.append(line.strip())

        return result


def parse_pair(path):
    with open(path) as f:
        lines = f.readlines()
        result = []
        for line in lines:
            line = line.strip()
            data, label = line.split()
            result.append([data, label])

        return result


def shuffle_data(data):
    idx = range(data.shape[0])
    np.random.shuffle(idx)

    shuffled_data = data[np.array(idx)]

    return shuffled_data


def read_images(filenames, image_size=256, normalize=True, one_chanel=False, long_type=False):
    images = []
    for fn in filenames:
        image = cv2.imread(fn)
        if image is None:
            image = np.zeros((image_size, image_size, 3), np.uint8)
            continue
            #print "Warning: File doesn't exist: ", fn

        image = cv2.resize(image, (image_size, image_size))

        if normalize:
            image = image.astype(np.float32) / 255.
        else:
            image = image.astype(np.float32)

        if one_chanel:
            image_t = np.zeros((image.shape[0], image.shape[1], 1), np.float32)
            image_t[:, :, 0] = image[:, :, 0]
            image = image_t

        if long_type:
            image = image.astype(np.long)

        image = image.transpose(2, 0, 1)
        images.append(image)

    images = np.stack(images)
    return images

def read_images_Ms(filenames, image_size=256, normalize=True, one_chanel=False, long_type=False):
    images = []
    images_128 = []
    images_64 = []
    for fn in filenames:
        image = cv2.imread(fn)
        if image is None:
            image = np.zeros((image_size, image_size, 3), np.uint8)
            print "Warning: File doesn't exist: ", fn

        image = cv2.resize(image, (image_size, image_size))
        image_128 = cv2.resize(image, (image_size/2, image_size/2))
        image_64 = cv2.resize(image, (image_size/4, image_size/4))
        if normalize:
            image = image.astype(np.float32) / 255.
            image_128 = image_128.astype(np.float32) / 255.
            image_64 = image_64.astype(np.float32) / 255.
        else:
            image = image.astype(np.float32)
            image_128 = image_128.astype(np.float32)
            image_64 = image_64.astype(np.float32) 
        if one_chanel:
            image_t = np.zeros((image.shape[0], image.shape[1], 1), np.float32)
            image_t[:, :, 0] = image[:, :, 0]
            image = image_t

        if long_type:
            image = image.astype(np.long)
            image_128 = image_128.astype(np.long)
            image_64 = image_64.astype(np.long)

        image = image.transpose(2, 0, 1)
        
        
        images.append(image)

        image_128 = image_128.transpose(2, 0, 1)
        
        
        images_128.append(image_128)

        image_64 = image_64.transpose(2, 0, 1)
        
        
        
        images_64.append(image_64)

    images = np.stack(images)
    images_128 = np.stack(images_128)
    images_64 = np.stack(images_64)

    return (images , images_128,images_64)



def read_labels_to_classes_chanels(filenames, n_class, image_size=256):
    maps = []
    for fn in filenames:
        image = cv2.imread(fn)
        if image is None:
            image = np.zeros((image_size, image_size, 3), np.uint8)
            print "Warning: File doesn't exist: ", fn

        image = cv2.resize(image, (image_size, image_size))

        value = image[:, :, 0]

        map = np.zeros((image.shape[0], image.shape[1], n_class), np.float32)
        for l in range(n_class):
            map[value == l, l] = 1

        map = map.transpose(2, 0, 1)
        maps.append(map)

    maps = np.stack(maps)
    return maps


def label_colorize(value):
    value = value[:, :, 0]

    r = value.copy()
    g = value.copy()
    b = value.copy()

    label_colors = np.array([
        [0, 0, 0],
        [255, 0, 0],
        [255, 51, 0],
        [255, 102, 0],
        [255, 153, 0],
        [255, 255, 0],
        [153, 255, 0],
        [0, 255, 0],
        [0, 255, 255],
        [0, 0, 255],
        [102, 0, 255],
        [255, 0, 255],
        [255, 0, 102]
    ])

    if np.max(value)+1 > label_colors.shape[0]:
        print "Error: label colorize only support ", label_colors.shape[0], "classes, but there are ", np.max(value)+1, "colors"

    for l in range(0, np.max(value)+1):
        r[value == l] = label_colors[l, 0]
        g[value == l] = label_colors[l, 1]
        b[value == l] = label_colors[l, 2]

    rgb = np.zeros((value.shape[0], value.shape[1], 3))
    rgb[:, :, 0] = b
    rgb[:, :, 1] = g
    rgb[:, :, 2] = r

    return rgb
