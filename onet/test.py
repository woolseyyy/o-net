import os
import argparse
from itertools import chain
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from torch.autograd import Variable
from dataset import *
from model import *
import resnet as rn
import scipy
import torchvision.models as t_models
from logger import Logger

parser = argparse.ArgumentParser(description='PyTorch implementation of O-Net')
parser.add_argument('--cuda', type=str, default='true', help='Set cuda usage')
parser.add_argument('--period_size', type=int, default=3, help='Set period times')
parser.add_argument('--epoch_size_list_A', type=str, default="50,5", help='Set epoch size in step A')
parser.add_argument('--epoch_size_list_B', type=str, default="1", help='Set epoch size in step B')
parser.add_argument('--batch_size', type=int, default=1, help='Set batch size')

parser.add_argument('--arch', type=str, default="ResSegnet", choices=["MySegnet2", "DeepUnet", "MySegnet", "Segnet", "FlatSegnet", "ResSegnet"],
                    help='Set architecture you wanna train: MySegnet2, DeepUnet, MySegnet, Segnet, FlatSegnet, ResSegnet')

parser.add_argument('--learning_seg_rate', type=float, default=0.003, help='Set learning rate for segnetor optimizer')
parser.add_argument('--learning_seg_momentum', type=float, default=0.3, help='Set learning momentum for segnetor')
parser.add_argument('--learning_seg_decay', type=float, default=0.005, help='Set learning weight decay for segnetor')
parser.add_argument('--learning_seg_sc_type', type=str, default=None,
                    choices=[None, "None", "step", "multi_step", "exponential"],
                    help='Set learning scheme: None, step, multi step, exponential are supoorted')
parser.add_argument('--learning_seg_sc_gama', type=float, default=0.1,
                    help='Set gama for segnetor learning scheme, only used in type step, multi step')
parser.add_argument('--learning_seg_sc_stepsize', type=int, default=1000,
                    help='Set step for segnetor learning scheme, only used in step')
parser.add_argument('--learning_seg_sc_milestones', type=str, default="1000,1500",
                    help='Set milestones for segnetor learning scheme, only used multi step')

parser.add_argument('--learning_gan_rate', type=float, default=0.0002, help='Set learning rate for gan optimizer')
parser.add_argument('--learning_gan_decay', type=float, default=0.00001, help='Set learning weight decay for gan')
parser.add_argument('--learning_gan_sc_type', type=str, default=None,
                    choices=[None, "None", "step", "multi_step", "exponential"],
                    help='Set learning scheme: None, step, multi step, exponential are supoorted')
parser.add_argument('--learning_gan_sc_gama', type=float, default=0.1,
                    help='Set gama for gan learning scheme, only used in type step, multi step')
parser.add_argument('--learning_gan_sc_stepsize', type=int, default=1000,
                    help='Set step for gan learning scheme, only used in step')
parser.add_argument('--learning_gan_sc_milestones', type=str, default="1000,1500",
                    help='Set milestones for gan learning scheme, only used multi step')

parser.add_argument('--learning_all_rate', type=float, default=0.000002, help='Set learning rate for all optimizer')
parser.add_argument('--learning_all_decay', type=float, default=0.00001, help='Set learning weight decay for all')
parser.add_argument('--learning_all_sc_type', type=str, default=None,
                    choices=[None, "None", "step", "multi_step", "exponential"],
                    help='Set learning scheme: None, step, multi step, exponential are supoorted')
parser.add_argument('--learning_all_sc_gama', type=float, default=0.1,
                    help='Set gama for all learning scheme, only used in type step, multi step')
parser.add_argument('--learning_all_sc_stepsize', type=int, default=1000,
                    help='Set step for all learning scheme, only used in step')
parser.add_argument('--learning_all_sc_milestones', type=str, default="1000,1500",
                    help='Set milestones for all learning scheme, only used multi step')

parser.add_argument('--class_weight', type=str, default=None, help='class weight')
parser.add_argument('--result_path', type=str, default='./results/',
                    help='Set the path the result images will be saved.')
parser.add_argument('--model_path', type=str, default='./models/', help='Set the path for trained models')
parser.add_argument('--logs_path', type=str, default='./logs', help='Set the path for logs')
parser.add_argument('--image_size', type=int, default=256, help='Image size. 256 for every experiment in the paper')
parser.add_argument('--n_classes', type=int, default=11, help='classes numbers')

parser.add_argument('--gan_curriculum', type=int, default=10000,
                    help='Strong GAN loss for certain period at the beginning')
parser.add_argument('--starting_rate', type=float, default=0.01,
                    help='Set the lambda weight between GAN loss and Recon loss during curriculum period at the beginning. We used the 0.01 weight.')
parser.add_argument('--default_rate', type=float, default=0.5,
                    help='Set the lambda weight between GAN loss and Recon loss after curriculum period. We used the 0.5 weight.')

parser.add_argument('--data_pair', type=str, default=None, required=True,
                    help='data in pair to train step A')
parser.add_argument('--data_unpair', type=str, default=None, required=True,
                    help='data un pair to train step B')
parser.add_argument('--test_pair', type=str, default=None, required=True,
                    help='test data in pair to do test')

parser.add_argument('--segnet_path', type=str, default=None,
                    help='segnet weights model path to load')
parser.add_argument('--generator_path', type=str, default=None,
                    help='generator weights model path to load')
parser.add_argument('--discriminator_path', type=str, default=None,
                    help='discriminator weights model path to load')

parser.add_argument('--update_interval_A', type=int, default=3, help='')
parser.add_argument('--update_interval_B', type=int, default=3, help='')
parser.add_argument('--log_interval_A', type=int, default=100, help='Print loss values every log_interval iterations.')
parser.add_argument('--log_interval_B', type=int, default=200, help='Print loss values every log_interval iterations.')
parser.add_argument('--image_save_interval_A', type=int, default=1000,
                    help='Save test results every image_save_interval iterations.')
parser.add_argument('--image_save_interval_B', type=int, default=1000,
                    help='Save test results every image_save_interval iterations.')
parser.add_argument('--model_save_interval_A', type=int, default=1000,
                    help='Save models every model_save_interval iterations.')
parser.add_argument('--model_save_interval_B', type=int, default=1000,
                    help='Save models every model_save_interval iterations.')


def as_np(data):
    return data.cpu().data.numpy()


def to_np(x):
    return x.data.cpu().numpy()


def get_fm_loss(real_feats, fake_feats, criterion):
    losses = 0
    for real_feat, fake_feat in zip(real_feats, fake_feats):
        l2 = (real_feat.mean(0) - fake_feat.mean(0)) * (real_feat.mean(0) - fake_feat.mean(0))
        loss = criterion(l2, Variable(torch.ones(l2.size())).cuda())
        losses += loss

    return losses


def get_gan_loss(dis_real, dis_fake, criterion, cuda):
    labels_dis_real = Variable(torch.ones([dis_real.size()[0], 1]))
    labels_dis_fake = Variable(torch.zeros([dis_fake.size()[0], 1]))
    labels_gen = Variable(torch.ones([dis_fake.size()[0], 1]))

    if cuda:
        labels_dis_real = labels_dis_real.cuda()
        labels_dis_fake = labels_dis_fake.cuda()
        labels_gen = labels_gen.cuda()

    dis_loss = criterion(dis_real, labels_dis_real) * 0.5 + criterion(dis_fake, labels_dis_fake) * 0.5
    gen_loss = criterion(dis_fake, labels_gen)

    return dis_loss, gen_loss


def cross_entropy2d(input, target, weight=None, size_average=True):
    n, c, h, w = input.size()
    input_1d = input.transpose(1, 2).transpose(2, 3).contiguous().view(-1, c)
    n, c, h, w = target.size()
    target_1d = target.transpose(1, 2).transpose(2, 3).contiguous().view(-1)

    return F.cross_entropy(input_1d, target_1d, weight, size_average)


def str_to_list(str):
    if str is None:
        return None

    int_str_list = str.split(",")
    int_list = []
    for int_str in int_str_list:
        int_list.append(int(int_str))

    return int_list


def str_to_list_float(str):
    if str is None:
        return None

    float_str_list = str.split(",")
    float_list = []
    for float_str in float_str_list:
        float_list.append(float(float_str))

    return float_list


def get_accuracy(produced, true):
    equal = torch.eq(produced.data, true.data)

    true_number = torch.sum(equal)
    total_number = torch.numel(equal)

    return 1. * true_number / total_number


def get_accuracy_perclass(produced, true, n_class):
    produced_np = as_np(produced)
    true_np = as_np(true)

    accuracy_list = []

    for class_i in range(n_class):
        class_true = true_np.copy()
        class_true[class_true != class_i] = -1
        class_true[class_true == class_i] = 1
        class_true[class_true == -1] = 0

        class_produced = produced_np.copy()
        class_produced[class_produced != class_i] = -1
        class_produced[class_produced == class_i] = 1

        sumup_array = class_true + class_produced

        true_number = np.sum(sumup_array == -1) + np.sum(sumup_array == 2)
        total_number = sumup_array.size

        if total_number == 0:
            accuracy = 1
        else:
            accuracy = 1. * true_number / total_number

        accuracy_list.append(accuracy)

    return accuracy_list


def get_IoU(produced, true, n_class):
    produced_np = as_np(produced)
    true_np = as_np(true)

    IoU_list = []

    for class_i in range(n_class):
        class_true = true_np.copy()
        class_true[class_true != class_i] = -1
        class_true[class_true == class_i] = 1
        class_true[class_true == -1] = 0

        class_produced = produced_np.copy()
        class_produced[class_produced != class_i] = -1
        class_produced[class_produced == class_i] = 1

        sumup_array = class_true + class_produced

        intersection = np.sum(sumup_array == 2)
        union = np.sum(sumup_array >= 0)

        if union == 0:
            IoU = 1
        else:
            IoU = 1. * intersection / union

        IoU_list.append(IoU)

    return IoU_list


def get_F1(produced, true, n_class):
    produced_np = as_np(produced)
    true_np = as_np(true)

    F1_list = []

    for class_i in range(n_class):
        class_true = true_np.copy()
        class_true[class_true != class_i] = -1
        class_true[class_true == class_i] = 1
        class_true[class_true == -1] = 0

        class_produced = produced_np.copy()
        class_produced[class_produced != class_i] = -1
        class_produced[class_produced == class_i] = 1

        sumup_array = class_true + class_produced

        TP = np.sum(sumup_array == 2)
        FP = np.sum(sumup_array == 1)
        FN = np.sum(sumup_array == 0)
        TN = np.sum(sumup_array == -1)

        if 2 * TP + FP + FN == 0:
            F1 = 1
        else:
            F1 = 2. * TP / (2 * TP + FP + FN)

        F1_list.append(F1)

    return F1_list


def get_ac_IoU_F1(produced, true, n_class):
    produced_np_all = as_np(produced)
    true_np_all= as_np(true)

    accuracy_list_all = []
    IoU_list_all = []
    F1_list_all = []

    for i in range(true_np_all.shape[0]):
        produced_np = produced_np_all[i, :, :, :]
        true_np = true_np_all[i, :, :, :]

        accuracy_list = []
        IoU_list = []
        F1_list = []

        for class_i in range(n_class):
            class_true = true_np.copy()
            class_true[class_true != class_i] = -1
            class_true[class_true == class_i] = 1
            class_true[class_true == -1] = 0

            class_produced = produced_np.copy()
            class_produced[class_produced != class_i] = -1
            class_produced[class_produced == class_i] = 1

            sumup_array = class_true + class_produced

            #accuracy
            true_number = np.sum(sumup_array == -1) + np.sum(sumup_array == 2)
            total_number = sumup_array.size

            if total_number == 0:
                accuracy = 1
            else:
                accuracy = 1. * true_number / total_number

            accuracy_list.append(accuracy)

            # IoU
            intersection = np.sum(sumup_array == 2)
            union = np.sum(sumup_array >= 0)

            if union == 0:
                IoU = 1
            else:
                IoU = 1. * intersection / union

            IoU_list.append(IoU)

            # F1
            TP = np.sum(sumup_array == 2)
            FP = np.sum(sumup_array == 1)
            FN = np.sum(sumup_array == 0)
            TN = np.sum(sumup_array == -1)

            if 2 * TP + FP + FN == 0:
                F1 = 1
            else:
                F1 = 2. * TP / (2 * TP + FP + FN)

            F1_list.append(F1)

        accuracy_list_all.append(accuracy_list)
        IoU_list_all.append(IoU_list)
        F1_list_all.append(F1_list)

    accuracy_list_result = np.mean(np.array(accuracy_list_all), 0)
    IoU_list_result = np.mean(np.array(IoU_list_all), 0)
    F1_list_result = np.mean(np.array(F1_list_all), 0)

    return accuracy_list_result.tolist(), IoU_list_result.tolist(), F1_list_result.tolist()


class EmptyLrSchema:
    def __init__(self, lr):
        self.lr = lr

    def step(self):
        pass

    def get_lr(self):
        return [self.lr]


def test(result_path, test_pair, cuda, segnet, generator):
    subdir_path =result_path

    batch_size = args.batch_size

    if os.path.exists(subdir_path):
        pass
    else:
        os.makedirs(subdir_path)

    test_number = test_pair.shape[0]
    n_batchs_test = test_number // batch_size

    accuracy_list = []
    per_accuracy_array = []
    IoU_array = []
    F1_array = []

    for test_batch_iter in range(n_batchs_test):
        # read test image
        test_image = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size, 0],
            args.image_size)
        test_label = read_labels_to_classes_chanels(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.n_classes, args.image_size)
        test_label_long = read_images(
            test_pair[test_batch_iter * batch_size:(test_batch_iter + 1) * batch_size:, 1],
            args.image_size, False, True, True)

        test_image = Variable(torch.FloatTensor(test_image), volatile=True)
        test_label = Variable(torch.FloatTensor(test_label), volatile=True)
        test_label_long = Variable(torch.LongTensor(test_label_long), volatile=True)

        if cuda:
            test_image = test_image.cuda()
            test_label = test_label.cuda()
            test_label_long = test_label_long.cuda()

        label_prob = segnet(test_image)
        max_val, label_produce = torch.max(label_prob, 1)
        label_produce = label_produce[:, np.newaxis, :, :]
        image_fake = generator(test_label)

        accuracy_list.append(get_accuracy(label_produce, test_label_long))
        per_accuracy_array_item, IoU_array_item, F1_array_item = get_ac_IoU_F1(label_produce, test_label_long, args.n_classes)
        per_accuracy_array.append(per_accuracy_array_item)
        IoU_array.append(IoU_array_item)
        F1_array.append(F1_array_item)

        n_testset = test_image.size()[0]

        for im_idx in range(n_testset):
            image_real_Val = test_image[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.
            label_produce_Val = label_colorize(
                label_produce[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            label_true_Val = label_colorize(test_label_long[im_idx].cpu().data.numpy().transpose(1, 2, 0))
            image_fake_Val = image_fake[im_idx].cpu().data.numpy().transpose(1, 2, 0) * 255.

            idx = test_batch_iter * batch_size + im_idx
            filename_prefix = os.path.join(subdir_path, str(idx))
            scipy.misc.imsave(filename_prefix + '.image.jpg',
                              image_real_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.label.jpg',
                              label_true_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.seg.jpg',
                              label_produce_Val.astype(np.uint8)[:, :, ::-1])
            scipy.misc.imsave(filename_prefix + '.gen.jpg', image_fake_Val.astype(np.uint8)[:, :, ::-1])

    accuracy = np.mean(np.array(accuracy_list))
    per_accuracy = np.mean(np.array(per_accuracy_array), 0)
    IoU = np.mean(np.array(IoU_array), 0)
    IoU_mean = np.mean(IoU)
    F1 = np.mean(np.array(F1_array), 0)
    F1_mean = np.mean(F1)

    print "Test----------------------------------------------"
    print "Accuracy:", accuracy
    for per_class in range(per_accuracy.shape[0]):
        print "\tClass", per_class, "accuracy:\t", per_accuracy[per_class]
    print "IoU:", IoU_mean
    for per_class in range(IoU.shape[0]):
        print "\tClass", per_class, "IoU:\t", IoU[per_class]
    print "F1:", F1_mean
    for per_class in range(F1.shape[0]):
        print "\tClass", per_class, "F1:\t", F1[per_class]
    print " "


def save_model(segnet, generator, discriminator, Step, period, epoch, iters, model_path):
    torch.save(segnet.state_dict(),
               os.path.join(model_path, str(period) + "-Step" + Step + "-" + str(iters) + '-segnet' + '.pkl'))
    torch.save(generator.state_dict(),
               os.path.join(model_path, str(period) + "-Step" + Step + "-" + str(iters) + '-generator' + '.pkl'))
    torch.save(discriminator.state_dict(),
               os.path.join(model_path, str(period) + "-Step" + Step + "-" + str(iters) + '-discriminator' + '.pkl'))
    print "save models at ", "period ", period, ", epoch " + Step + " ", epoch, ", iteration " + Step + " ", iters
    print "store in ", model_path
    print " "


def learning_rate_producer(optimizer, method, step_size=0, milestones=None, gamma=0.1, last_epoch=-1):
    if milestones is None:
        milestones = []
    if method is None or method == "None":
        return EmptyLrSchema(optimizer.param_groups[0]['lr'])
    elif method == "step":
        return lr_scheduler.StepLR(optimizer, step_size, gamma, last_epoch)
    elif method == 'multi_step':
        return lr_scheduler.MultiStepLR(optimizer, milestones, gamma, last_epoch)
    elif method == "exponential":
        return lr_scheduler.ExponentialLR(optimizer, gamma, last_epoch)


def log_print(Step, period, epoch, iters, iters_all, seg_loss, gen_loss_total, gen_loss, fm_loss, recon_loss, dis_loss,
              lr_manager_seg, lr_manager_gen, lr_manager_dis, lr_manager_all):
    print "Step-" + Step + "---------------------"
    print "period:", period, "\tepoch:", epoch
    print "iteration " + Step + ":", iters, "\titeration:", iters_all
    if seg_loss is not None:
        print "\tSEG Loss:", as_np(seg_loss.mean())
    print "\tGEN Total Loss:", as_np(gen_loss_total.mean())
    print "\tGEN Loss:", as_np(gen_loss.mean())
    print "\tFeature Matching Loss:", as_np(fm_loss.mean())
    print "\tRECON Loss:", as_np(recon_loss.mean())
    print "\tDIS Loss:", as_np(dis_loss.mean())
    if lr_manager_seg is not None:
        print "\tSegmentor Learning Rate:", lr_manager_seg.get_lr()[0]
    if lr_manager_gen is not None:
        print "\tGenerator Learning Rate:", lr_manager_gen.get_lr()[0]
    print "\tDiscriminator Learning Rate:", lr_manager_dis.get_lr()[0]
    if lr_manager_all is not None:
        print "\tAll O-Net Learning Rate:", lr_manager_all.get_lr()[0]
    print " "


def log_tensorboard(Step, logger, gen_loss_total, seg_loss, dis_loss, iters, segnet, generator, discriminator):
    if Step == 'A':
        info = {
            'gen_loss_total_A': as_np(gen_loss_total.mean())[0].item(),
            'seg_loss_A': as_np(seg_loss.mean())[0].item(),
            'dis_loss_A': as_np(dis_loss.mean())[0].item()
        }
    elif Step == 'B':
        info = {
            'gen_loss_total_B': as_np(gen_loss_total.mean())[0].item(),
            'dis_loss_B': as_np(dis_loss.mean())[0].item()
        }
    else:
        info = {}

    for tag, value in info.items():
        logger.scalar_summary(tag, value, iters)

    for tag, value in segnet.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)

    for tag, value in generator.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)

    for tag, value in discriminator.named_parameters():
        tag = tag.replace('.', '/')
        logger.histo_summary(tag, to_np(value), iters)
        if value.grad is None:
            print tag, "has no grad, please check your model"
        else:
            logger.histo_summary(tag + '/grad', to_np(value.grad), iters)


def main():
    global args
    args = parser.parse_args()

    cuda = args.cuda
    if cuda == 'true':
        cuda = True
    else:
        cuda = False

    if args.class_weight is not None:
        class_weight = np.array(str_to_list_float(args.class_weight))
        class_weight = Variable(torch.FloatTensor(class_weight))
        if cuda:
            class_weight = class_weight.cuda()
    else:
        class_weight = None

    logger = Logger(args.logs_path)

    result_path = args.result_path
    model_path = args.model_path

    data_pair, data_unpair, test_pair = get_data(args.data_pair, args.data_unpair, args.test_pair)

    # test_image = read_images(test_pair[:, 0], args.image_size)
    # test_label = read_images(test_pair[:, 1], args.image_size, False, True)
    #
    # test_image = Variable(torch.FloatTensor(test_image), volatile=True)
    # test_label = Variable(torch.FloatTensor(test_label), volatile=True)


    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_path):
        os.makedirs(model_path)

    if args.arch == "Segnet":
        segnet = Segnet()
    elif args.arch == "MySegnet":
        segnet = MySegnet()
    elif args.arch == "DeepUnet":
        segnet = DeepUnet()
    elif args.arch == "MySegnet2":
        segnet = MySegnet2()
    elif args.arch == "FlatSegnet":
        segnet = FlatSegnet()
    elif args.arch == "ResSegnet":
        segnet = rn.ResSegNet_BK(rn.Bottleneck, [2, 2, 2, 2])
    else:
        print "Unknown architecture, use Mysegnet2 default"
        segnet = MySegnet2()

    generator = Generator(args.n_classes)
    discriminator = Discriminator()

    # load weights
    if args.segnet_path is not None:
        if os.path.isfile(args.segnet_path):
            print "loading segnet weights from:", args.segnet_path
            segnet.load_state_dict(torch.load(args.segnet_path))
        else:
            print "Warning: File not exit:", args.segnet_path

    if args.generator_path is not None:
        if os.path.isfile(args.generator_path):
            print "loading generator weights from:", args.generator_path
            generator.load_state_dict(torch.load(args.generator_path))
        else:
            print "Warning: File not exit:", args.generator_path

    if args.discriminator_path is not None:
        if os.path.isfile(args.discriminator_path):
            print "loading discriminator weights from:", args.discriminator_path
            discriminator.load_state_dict(torch.load(args.discriminator_path))
        else:
            print "Warning: File not exit:", args.discriminator_path

    if cuda:
        segnet = segnet.cuda()
        generator = generator.cuda()
        discriminator = discriminator.cuda()


    test(result_path, test_pair, cuda, segnet, generator)


if __name__ == "__main__":
    main()
