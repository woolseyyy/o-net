import torch.nn as nn
import torch
import math
import torch.utils.model_zoo as model_zoo
import model_utils
import torchvision as tv
from torch.autograd import Variable
import numpy as np
import torch.nn.functional as F

__all__ = ['ResNet', 'resnet18', 'resnet34', 'resnet50', 'resnet101',
           'resnet152']


model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}


def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class ResNet(nn.Module):

    def __init__(self, block, layers, num_classes=1000):
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        print x.size()
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        print x.size()

        x = self.layer1(x)
        print x.size()
        x = self.layer2(x)
        print x.size()
        x = self.layer3(x)
        print x.size()
        x = self.layer4(x)
        print x.size()

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        ss
        return x


def resnet18(pretrained=False, **kwargs):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(BasicBlock, [2, 2, 2, 2], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet18']))
    return model


def resnet34(pretrained=False, **kwargs):
    """Constructs a ResNet-34 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(BasicBlock, [3, 4, 6, 3], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet34']))
    return model


def resnet50(pretrained=False, **kwargs):
    """Constructs a ResNet-50 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(Bottleneck, [3, 4, 6, 3], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet50']))
    return model


def resnet101(pretrained=False, **kwargs):
    """Constructs a ResNet-101 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(Bottleneck, [3, 4, 23, 3], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet101']))
    return model


def resnet152(pretrained=False, **kwargs):
    """Constructs a ResNet-152 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(Bottleneck, [3, 8, 36, 3], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet152']))
    return model















class ResSegNet(nn.Module):

    def __init__(self, block, layers, num_classes=11):
        self.inplanes = 64
        super(ResSegNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        

        self.up7 = Up1In(512, 512)
        self.up6 = Up(512, 256)
        self.up5 = Up(256, 128)
        self.up4 = Up(128, 64)
        self.up3 = Up(64, 64)
        self.up2 = Up(64, 64)
        self.up1 = Up(64, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init)

        print "down1",down1.size()
        down2 = self.maxpool(down1)

        print "down2",down2.size()
        down3 = self.layer1(down2)
        print "down3",down3.size()
        s
        down4 = self.layer2(down3)
        #print "down4",down4.size()
        down5 = self.layer3(down4)
        #print "down5",down5.size()
        down6 = self.layer4(down5)
        #print "down6",down6.size()
        down7 = self.avgpool(down6)


        #print "down7",down7.size()
        
        up7= self.up7(down7) 
        up6= self.up6(down6 ,up7)
        
        up5= self.up5(down5 ,up6)
        up4= self.up4(down4 ,up5)
        up3= self.up3(down3 ,up4)
        up2= self.up2(down2 ,up3)
        up1= self.up1(down1 ,up2)
        result=self.final(up1)
        #print "result",result.size()
        return result


class ResSegNet_BK(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 64
        super(ResSegNet_BK, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        

        self.up7 = Up1In(2048, 2048)
        self.up6 = Up(2048, 1024)
        self.up5 = Up(1024, 512)
        self.up4 = Up(512, 256)
        self.up3 = Up(256, 64)
        self.up2 = Up(64, 64)
        self.up1 = Up(64, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init)

        #print "down1",down1.size()
        down2 = self.maxpool(down1)

        # print "down2",down2.size()
        
        down3 = self.layer1(down2)
        #print "down3",down3.size()
        down4 = self.layer2(down3)
        #print "down4",down4.size()
        down5 = self.layer3(down4)
        #print "down5",down5.size()
        down6 = self.layer4(down5)
        #print "down6",down6.size()
        down7 = self.avgpool(down6)


        #print "down7",down7.size()
        
        up7= self.up7(down7)        
        up6= self.up6(down6 ,up7)
        up5= self.up5(down5 ,up6)
        up4= self.up4(down4 ,up5)
        up3= self.up3(down3 ,up4)
        up2= self.up2(down2 ,up3)
        up1= self.up1(down1 ,up2)
        result=self.final(up1)
        #print "result",result.size()
        return result


class ResSegNet_MS_BK(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 192
        super(ResSegNet_MS_BK, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.conv2 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0], stride=2)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        

        
        self.up7 = Up1In(2048, 1024)
        self.up6 = Up(1024, 512)
        self.up5 = Up(512, 256)
        self.up4 = Up(256, 192)
        self.up3 = Up(192, 128)
        self.up2 = Up(128, 32)
        #self.up1 = Up(128, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)
        
        

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))

            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x, x1,x2):

        x1=self.conv2(x1)
        x2=self.conv2(x2)
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init) 

        down1=torch.cat((down1,x1),dim=1)

        print "down1",down1.size()
        down2 = self.maxpool(down1)
        down2=torch.cat((down2,x2),dim=1)

        print "down2",down2.size()
        
        # print down2.size()
        down3 = self.layer1(down2)
        # print down3.size()
        print "down3",down3.size()
        s
        down4 = self.layer2(down3)
        # print "down4",down4.size()
        down5 = self.layer3(down4)
        # print "down5",down5.size()
        down6 = self.layer4(down5)
        # print "down6",down6.size()
        # down7 = self.avgpool(down6)
        

        # print "down7",down7.size()
        
        up7= self.up7(down6)
        # up7= self.up7(up7)
        # print "up7" , up7.size() 
        # print "down5 ,up7", down5.size() ,up7.size()

        up6= self.up6(down5 ,up7)
        # print "up6" , up6.size()
        # print "down5 ,up7", down4.size() ,up6.size()
        up5= self.up5(down4 ,up6)
        # print "up5" , up5.size() 
        # print "down5 ,up7", down3.size() ,up5.size()       
        up4= self.up4(down3 ,up5)
        # print "up4" , up4.size() 
        # print "down5 ,up7", down2.size() ,up4.size()       
        up3= self.up3(down2 ,up4)
        # print "up3" , up3.size()   
        # print "down5 ,up7", down1.size() ,up3.size()     
        up2= self.up2(down1 ,up3)
        # print "up2" , up2.size()   
            
        #up1= self.up1(down1 ,up2)
        #print "up1" , up1.size() 
        result=self.final(up2)
       
        # print "result",result.size()
       
        return result, down6
    def FE_weights(self,paramdict , extract=True):


        # Dont use it 
        if (extract):
            # print self.state_dict().keys()
            keys=self.state_dict().keys()
            # print dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38]).keys()
            
            return  dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-33])
        else:
            keys=paramdict.keys()
            vals=self.state_dict()
            for key in keys:
                vals[key].copy_(paramdict[key])
            self.load_state_dict(vals)
    def FE_parameters(self):

        params=list(self.children())[:-7]
        print params
        s
        
        return nn.Sequential(*params).parameters()
    def labeling_grad(self , Val=True):
        params=list(self.children())[-7:]
        
        for par in nn.Sequential(*params).parameters():
            par.require_grad=Val 

        

         #Variable(torch.FloatTensor(params))
        # if (extract):
        #     # print self.state_dict().keys()
        #     keys=self.state_dict().keys()
        #     print self.parameters()[keys[0]]
        #     s
        #     print keys
        #     s
        #     for key in keys:
        #         print key
        #     print keys
        #     s
        #     print dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38]).keys()
            
        #     return  dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38])
        # else:
        #     keys=paramdict.keys()
        #     vals=self.state_dict()
        #     for key in keys:
        #         vals[key].copy_(paramdict[key])
        #     self.load_state_dict(vals)

class ResSegNet_Mix(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 256
        super(ResSegNet_Mix, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.conv2 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.conv3 = nn.Conv2d(3, 128, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0], stride=2)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        

        
        self.up7 = Up1In(2048, 1024)
        self.up6 = Up(1024, 512)
        self.up5 = Up(512, 256)
        self.up4 = Up(256, 256)
        self.up3 = Up(256, 128)
        self.up2 = Up(128, 32)
        #self.up1 = Up(128, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)
        
        

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))

            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x, x1,x2):

        x1=self.conv2(x1)
        x2=self.conv3(x2)
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init) 

        #down1=down1+x1
        down1=torch.cat((down1,x1),dim=1)
        # print "down1",down1.size()
        down2 = self.maxpool(down1)
        down2=torch.cat((down2,x2),dim=1)

        # print "down2",down2.size()
        
        # print down2.size()
        down3 = self.layer1(down2)
        # print down3.size()
        # print "down3",down3.size()
        
        down4 = self.layer2(down3)
        # print "down4",down4.size()
        down5 = self.layer3(down4)
        # print "down5",down5.size()
        down6 = self.layer4(down5)
        # print "down6",down6.size()
        # down7 = self.avgpool(down6)
        

        # print "down7",down7.size()
        
        up7= self.up7(down6)
        # up7= self.up7(up7)
        # print "up7" , up7.size() 
        # print "down5 ,up7", down5.size() ,up7.size()

        up6= self.up6(down5 ,up7)
        # print "up6" , up6.size()
        # print "down5 ,up7", down4.size() ,up6.size()
        up5= self.up5(down4 ,up6)
        # print "up5" , up5.size() 
        # print "down5 ,up7", down3.size() ,up5.size()       
        up4= self.up4(down3 ,up5)
        # print "up4" , up4.size() 
        # print "down5 ,up7", down2.size() ,up4.size()       
        up3= self.up3(down2 ,up4)
        # print "up3" , up3.size()   
        # print "down5 ,up7", down1.size() ,up3.size()     
        up2= self.up2(down1 ,up3)
        # print "up2" , up2.size()   
            
        #up1= self.up1(down1 ,up2)
        # print "up1" , up1.size() 
        result=self.final(up2)
       
        # print "result",result.size()
        
        return result, [down2 ,down1]
    def FE_weights(self,paramdict , extract=True):


        # Dont use it 
        if (extract):
            # print self.state_dict().keys()
            keys=self.state_dict().keys()
            # print dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38]).keys()
            
            return  dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-33])
        else:
            keys=paramdict.keys()
            vals=self.state_dict()
            for key in keys:
                vals[key].copy_(paramdict[key])
            self.load_state_dict(vals)
    def FE_parameters(self):

        params=list(self.children())[:-7]
        
        
        return nn.Sequential(*params).parameters()
    def labeling_grad(self , Val=True):
        params=list(self.children())[-7:]
        
        for par in nn.Sequential(*params).parameters():
            par.require_grad=Val
    def FE_grad(self , Val=True):
        params=list(self.children())[:-7]
        
        for par in nn.Sequential(*params).parameters():
            par.require_grad=Val  

        

         #Variable(torch.FloatTensor(params))
        # if (extract):
        #     # print self.state_dict().keys()
        #     keys=self.state_dict().keys()
        #     print self.parameters()[keys[0]]
        #     s
        #     print keys
        #     s
        #     for key in keys:
        #         print key
        #     print keys
        #     s
        #     print dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38]).keys()
            
        #     return  dict(( key ,self.state_dict()[key] ) for key in keys[0:len(keys)-38])
        # else:
        #     keys=paramdict.keys()
        #     vals=self.state_dict()
        #     for key in keys:
        #         vals[key].copy_(paramdict[key])
        #     self.load_state_dict(vals)

class Gen_ResSegNet(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 192
        super(Gen_ResSegNet, self).__init__()
        self.conv1 = nn.Conv2d(num_classes, 128, kernel_size=7, stride=2, padding=3,
                               bias=False)
        
        self.bn1 = nn.BatchNorm2d(128)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        
        self.conv2 = nn.Conv2d(128, 192, kernel_size=3, stride=1, padding=1,
                               bias=False)
        

        self.layer1 = self._make_layer(block, 64, layers[0], stride=2)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        

        
        self.up7 = Up1In(2048, 1024)
        self.up6 = Up(1024, 512)
        self.up5 = Up(512, 256)
        self.up4 = Up(256, 192)
        self.up3 = Up(192, 128)
        self.up2 = Up(128, 32)

        self.final = nn.Conv2d(32, 3, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.valid=nn.Sigmoid()

        
        

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))

            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x ,  skipinput):

        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init) 

        #down1=torch.cat((down1,down1),dim=1)

        # print "down1",down1.size()
        down2 = self.maxpool(down1)
        down2 = self.conv2(down2)
        #down2=torch.cat((down2,down2),dim=1)

        # print "down2",down2.size()
        # print down2.size()
        down3 = self.layer1(down2)
        # print down3.size()
        # print "down3",down3.size()
        down4 = self.layer2(down3)
        # print "down4",down4.size()
        down5 = self.layer3(down4)
        # print "down5",down5.size()
        down6 = self.layer4(down5)
        # print "down6",down6.size()
        

        # print "down7",down7.size()
        if skipinput is not None:
            down6=down6+skipinput
        
        up7= self.up7(down6)
        
        # print "up7" , up7.size()
        # print down5.size() , up7.size()        
        up6= self.up6(down5 ,up7)
        
        # print "up6" , up6.size()
        # print down5.size() ,up6.size()
        up5= self.up5(down4 ,up6)
        
        # print "up5" , up5.size()        
        # print down3.size() ,up5.size()
        up4= self.up4(down3 ,up5)
        
        # print "up4" , up4.size()        
        # print down2.size() ,up4.size()
        up3= self.up3(down2 ,up4)
        
        # print "up3" , up3.size()
        

        # print down1.size() ,up3.size()
        up2= self.up2(down1 ,up3)
        
        

        result=self.final(up2)
               
        result = self.valid(result)
        # print "result",result.size()
        
        return result
    def R_parameters(self):

        params=list(self.children())[9:]

        return nn.Sequential(*params).parameters()
    def labeling_grad(self , Val=True):
        params=list(self.children())[:9]
        
        for par in nn.Sequential(*params).parameters():
            par.require_grad=Val




class ResSegNet_MS_Mc_cat_BK(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 64
        super(ResSegNet_MS_Mc_cat_BK, self).__init__()
        self.conv1 = nn.Conv2d(5, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 128, layers[0])
        self.layer2 = self._make_layer(block, 256, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 512, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 1024, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        

        self.up7 = Up1In(4096, 4096)
        self.up6 = Up(4096, 2048)
        self.up5 = Up(2048, 1024)
        self.up4 = Up(1024, 512)
        self.up3 = Up(512, 64)
        self.up2 = Up(64, 64)
        self.up1 = Up(64, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)
        
        self.conv2 = nn.Conv2d(5, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.conv3 = nn.Conv2d(5, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x , x1,x2):

        x1=self.conv2(x1)
        x2=self.conv3(x2)
        x1=self.relu(x1)
        x2=self.relu(x2)
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init) 

        down1=down1+x1

        # print "down1",down1.size()
        down2 = self.maxpool(down1)
        down2=down2+x2

        # print "down2",down2.size()
        # print down2.size()
        down3 = self.layer1(down2)
        # print down3.size()
        # print "down3",down3.size()
        down4 = self.layer2(down3)
        # print "down4",down4.size()
        down5 = self.layer3(down4)
        # print "down5",down5.size()
        down6 = self.layer4(down5)
        # print "down6",down6.size()
        down7 = self.avgpool(down6)


        # print "down7",down7.size()
        
        up7= self.up7(down7)        
        up6= self.up6(down6 ,up7)
        up5= self.up5(down5 ,up6)
        up4= self.up4(down4 ,up5)
        up3= self.up3(down3 ,up4)
        up2= self.up2(down2 ,up3)
        up1= self.up1(down1 ,up2)
        result=self.final(up1)
        #print "result",result.size()
        return result


class ResSegNet_MS_Mc_BK(nn.Module):

    def __init__(self, block, layers, num_classes=6):
        self.inplanes = 192
        super(ResSegNet_MS_Mc_BK, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 256, layers[0])
        self.layer2 = self._make_layer(block, 512, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 512, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 1024, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        

        self.up7 = Up1In(4096, 4096)
        self.up6 = Up(4096, 2048)
        self.up5 = Up(2048, 2048)
        self.up4 = Up(2048, 1024)
        self.up3 = Up(1024, 192)
        self.up2 = Up(192, 128)
        self.up1 = Up(128, 32)
        self.final = nn.Conv2d(32, num_classes, kernel_size=3, stride=1, padding=1,
                               bias=False)
        
        self.conv2 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.conv3 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x , x1,x2):

        x1=self.conv2(x1)
        x2=self.conv3(x2)
        init = self.conv1(x)
        init = self.bn1(init)
        down1 = self.relu(init) 

        down1=torch.cat((down1,x1),dim=1)

        # print "down1",down1.size()
        down2 = self.maxpool(down1)
        down2=torch.cat((down2,x2),dim=1)

        # print "down2",down2.size()
        # print down2.size()
        down3 = self.layer1(down2)
        # print down3.size()
        # print "down3",down3.size()
        down4 = self.layer2(down3)
        #print "down4",down4.size()
        down5 = self.layer3(down4)
        #print "down5",down5.size()
        down6 = self.layer4(down5)
        #print "down6",down6.size()
        down7 = self.avgpool(down6)


        #print "down7",down7.size()
        
        up7= self.up7(down7)        
        up6= self.up6(down6 ,up7)
        up5= self.up5(down5 ,up6)
        up4= self.up4(down4 ,up5)
        up3= self.up3(down3 ,up4)
        up2= self.up2(down2 ,up3)
        up1= self.up1(down1 ,up2)
        result=self.final(up1)
        #print "result",result.size()
        return result

class S_Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(S_Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2_1 = nn.Conv2d(planes, planes, kernel_size=(3,1), stride=(stride,1),
                               padding=(1,0), bias=False)
        self.conv2_2 = nn.Conv2d(planes, planes, kernel_size=(1,3), stride=(1,stride),
                               padding=(0,1), bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        #print "0out",out.size()
        out = self.conv2_1(out)
        #print "1out",out.size()
        out = self.conv2_2(out)
        #print "2out",out.size()
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class Up1In(nn.Module):
    def __init__(self, in_size, out_size, is_deconv=True):
        super(Up1In, self).__init__()
        self.conv = nn.Conv2d(out_size, out_size, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(out_size)
        self.out_size = out_size
        if is_deconv:
            self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2, stride=2)
        else:
            self.up = nn.Upsample(scale_factor=2)

    def forward(self, inputs):
        outputs = self.up(inputs)
        outputs = self.bn1(outputs)

        #print "output size",outputs.size()
        return self.bn1(self.conv(outputs))
      
class Up(nn.Module):
    def __init__(self, in_size, out_size, is_deconv=False):
        super(Up, self).__init__()
        self.conv = nn.Conv2d(in_size, out_size, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(out_size)
        if is_deconv:
            self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2, stride=2)
        else:
            self.up = nn.Upsample(scale_factor=2)

    def forward(self, inputs1, inputs2):
        
        
        outputs2 = self.up(inputs2)
        offset = outputs2.size()[2] - inputs1.size()[2]
        padding = 2 * [offset // 2, offset // 2]
        outputs1 = F.pad(inputs1, padding)
        res = self.conv(outputs1 + outputs2)
        return self.bn1(res)
        
