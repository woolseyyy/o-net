import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable
import ipdb

import numpy as np

import model_utils

kernel_sizes = [4, 3, 3]
strides = [2, 2, 1]
paddings = [0, 0, 1]

latent_dim = 300


class Discriminator(nn.Module):
    def __init__(
            self,
    ):
        super(Discriminator, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, 4, 2, 1, bias=False)
        self.relu1 = nn.LeakyReLU(0.2, inplace=True)

        self.conv2 = nn.Conv2d(64, 64 * 2, 4, 2, 1, bias=False)
        self.bn2 = nn.BatchNorm2d(64 * 2)
        self.relu2 = nn.LeakyReLU(0.2, inplace=True)

        self.conv3 = nn.Conv2d(64 * 2, 64 * 4, 4, 2, 1, bias=False)
        self.bn3 = nn.BatchNorm2d(64 * 4)
        self.relu3 = nn.LeakyReLU(0.2, inplace=True)

        self.conv4 = nn.Conv2d(64 * 4, 64 * 8, 4, 2, 1, bias=False)
        self.bn4 = nn.BatchNorm2d(64 * 8)
        self.relu4 = nn.LeakyReLU(0.2, inplace=True)

        self.conv5 = nn.Conv2d(64 * 8, 64 * 16, 4, 2, 1, bias=False)
        self.bn5 = nn.BatchNorm2d(64 * 16)
        self.relu5 = nn.LeakyReLU(0.2, inplace=True)

        self.conv6 = nn.Conv2d(64 * 16, 64 * 32, 4, 2, 1, bias=False)
        self.bn6 = nn.BatchNorm2d(64 * 32)
        self.relu6 = nn.LeakyReLU(0.2, inplace=True)

        self.conv7 = nn.Conv2d(64 * 32, 1, 4, 1, 0, bias=False)

    def forward(self, input):
        conv1 = self.conv1(input)
        relu1 = self.relu1(conv1)

        conv2 = self.conv2(relu1)
        bn2 = self.bn2(conv2)
        relu2 = self.relu2(bn2)

        conv3 = self.conv3(relu2)
        bn3 = self.bn3(conv3)
        relu3 = self.relu3(bn3)

        conv4 = self.conv4(relu3)
        bn4 = self.bn4(conv4)
        relu4 = self.relu4(bn4)

        conv5 = self.conv5(relu4)
        bn5 = self.bn5(conv5)
        relu5 = self.relu5(bn5)

        conv6 = self.conv6(relu5)
        bn6 = self.bn6(conv6)
        relu6 = self.relu6(bn6)

        conv7 = self.conv7(relu6)

        return torch.sigmoid(conv7), [relu4, relu5, relu6]


class Generator(nn.Module):
    def __init__(
            self,
            n_classes,
            extra_layers=False
    ):

        super(Generator, self).__init__()

        if extra_layers == True:
            self.main = nn.Sequential(
                nn.Conv2d(n_classes, 64, 4, 2, 1, bias=False),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64, 64 * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 2),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 2, 64 * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 4),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 4, 64 * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 8),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 8, 64 * 16, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 16),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 16, 64 * 32, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 32),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 32, 100, 4, 1, 0, bias=False),
                nn.BatchNorm2d(100),
                nn.LeakyReLU(0.2, inplace=True),

                nn.ConvTranspose2d(100, 64 * 32, 4, 1, 0, bias=False),
                nn.BatchNorm2d(64 * 32),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 32, 64 * 16, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 16),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 16, 64 * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 8),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 8, 64 * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 4),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 4, 64 * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 2),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 2, 64, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64),
                nn.ReLU(True),
                nn.ConvTranspose2d(64, 3, 4, 2, 1, bias=False),
                nn.Sigmoid()
            )

        if extra_layers == False:
            self.main = nn.Sequential(
                nn.Conv2d(n_classes, 64, 4, 2, 1, bias=False),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64, 64 * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 2),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 2, 64 * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 4),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 4, 64 * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 8),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 8, 64 * 16, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 16),
                nn.LeakyReLU(0.2, inplace=True),
                nn.Conv2d(64 * 16, 64 * 32, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 32),
                nn.LeakyReLU(0.2, inplace=True),

                nn.ConvTranspose2d(64 * 32, 64 * 16, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 16),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 16, 64 * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 8),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 8, 64 * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 4),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 4, 64 * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64 * 2),
                nn.ReLU(True),
                nn.ConvTranspose2d(64 * 2, 64, 4, 2, 1, bias=False),
                nn.BatchNorm2d(64),
                nn.ReLU(True),
                nn.ConvTranspose2d(64, 3, 4, 2, 1, bias=False),
                nn.Sigmoid()
            )

    def forward(self, input):
        return self.main(input)


class MySegnet(nn.Module):
    def __init__(self, n_classes=11, in_channels=3, is_unpooling=True):
        super(MySegnet, self).__init__()

        self.in_channels = in_channels
        self.is_unpooling = is_unpooling

        self.down1 = model_utils.MysegnetDown(self.in_channels, 32)
        self.down2 = model_utils.MysegnetDown(32, 32)
        self.down3 = model_utils.MysegnetDown(32, 64)
        self.down4 = model_utils.MysegnetDown(64, 64)
        self.down5 = model_utils.MysegnetDown(64, 64)
        self.down6 = model_utils.MysegnetDown(64, 128)
        self.down7 = model_utils.MysegnetDown(128, 128)

        self.up7 = model_utils.segnetUp3(128, 128)
        self.up6 = model_utils.segnetUp3(128, 64)
        self.up5 = model_utils.segnetUp3(64, 64)
        self.up4 = model_utils.segnetUp3(64, 64)
        self.up3 = model_utils.segnetUp3(64, 32)
        self.up2 = model_utils.segnetUp2(32, 32)
        self.up1 = model_utils.segnetUp2(32, n_classes)

    def forward(self, inputs):
        down1, indices_1, unpool_shape1 = self.down1(inputs)
        down2, indices_2, unpool_shape2 = self.down2(down1)
        down3, indices_3, unpool_shape3 = self.down3(down2)
        down4, indices_4, unpool_shape4 = self.down4(down3)
        down5, indices_5, unpool_shape5 = self.down5(down4)
        down6, indices_6, unpool_shape6 = self.down6(down5)
        down7, indices_7, unpool_shape7 = self.down7(down6)

        up7 = self.up7(down7, indices_7, unpool_shape7)
        up6 = self.up6(up7, indices_6, unpool_shape6)
        up5 = self.up5(up6, indices_5, unpool_shape5)
        up4 = self.up4(up5, indices_4, unpool_shape4)
        up3 = self.up3(up4, indices_3, unpool_shape3)
        up2 = self.up2(up3, indices_2, unpool_shape2)
        up1 = self.up1(up2, indices_1, unpool_shape1)

        return up1


class Segnet(nn.Module):
    def __init__(self, n_classes=11, in_channels=3, is_unpooling=True):
        super(Segnet, self).__init__()

        self.in_channels = in_channels
        self.is_unpooling = is_unpooling

        self.down1 = model_utils.segnetDown2(self.in_channels, 64)
        self.down2 = model_utils.segnetDown2(64, 128)
        self.down3 = model_utils.segnetDown3(128, 256)
        self.down4 = model_utils.segnetDown3(256, 512)
        self.down5 = model_utils.segnetDown3(512, 512)

        self.up5 = model_utils.segnetUp3(512, 512)
        self.up4 = model_utils.segnetUp3(512, 256)
        self.up3 = model_utils.segnetUp3(256, 128)
        self.up2 = model_utils.segnetUp2(128, 64)
        self.up1 = model_utils.segnetUp2(64, n_classes)

    def forward(self, inputs):

        down1, indices_1, unpool_shape1 = self.down1(inputs)
        down2, indices_2, unpool_shape2 = self.down2(down1)
        down3, indices_3, unpool_shape3 = self.down3(down2)
        down4, indices_4, unpool_shape4 = self.down4(down3)
        down5, indices_5, unpool_shape5 = self.down5(down4)

        up5 = self.up5(down5, indices_5, unpool_shape5)
        up4 = self.up4(up5, indices_4, unpool_shape4)
        up3 = self.up3(up4, indices_3, unpool_shape3)
        up2 = self.up2(up3, indices_2, unpool_shape2)
        up1 = self.up1(up2, indices_1, unpool_shape1)

        return up1

    def init_vgg16_params(self, vgg16):
        blocks = [self.down1,
                  self.down2,
                  self.down3,
                  self.down4,
                  self.down5]

        ranges = [[0, 4], [5, 9], [10, 16], [17, 23], [24, 29]]
        features = list(vgg16.features.children())

        vgg_layers = []
        for _layer in features:
            if isinstance(_layer, nn.Conv2d):
                vgg_layers.append(_layer)

        merged_layers = []
        for idx, conv_block in enumerate(blocks):
            if idx < 2:
                units = [conv_block.conv1.cbr_unit,
                         conv_block.conv2.cbr_unit]
            else:
                units = [conv_block.conv1.cbr_unit,
                         conv_block.conv2.cbr_unit,
                         conv_block.conv3.cbr_unit]
            for _unit in units:
                for _layer in _unit:
                    if isinstance(_layer, nn.Conv2d):
                        merged_layers.append(_layer)

        assert len(vgg_layers) == len(merged_layers)

        for l1, l2 in zip(vgg_layers, merged_layers):
            if isinstance(l1, nn.Conv2d) and isinstance(l2, nn.Conv2d):
                assert l1.weight.size() == l2.weight.size()
                assert l1.bias.size() == l2.bias.size()
                l2.weight.data = l1.weight.data
                l2.bias.data = l1.bias.data


class FlatSegnet(nn.Module):
    def __init__(self, n_classes=11, in_channels=3, is_unpooling=True):
        super(FlatSegnet, self).__init__()

        self.in_channels = in_channels
        self.is_unpooling = is_unpooling

        self.down1 = model_utils.segnetDown2(self.in_channels, 64)
        self.down2 = model_utils.segnetDown2(64, 64)
        self.down3 = model_utils.segnetDown3(64, 64)
        self.down4 = model_utils.segnetDown3(64, 64)
        self.down5 = model_utils.segnetDown3(64, 64)

        self.up5 = model_utils.segnetUp3(64, 64)
        self.up4 = model_utils.segnetUp3(64, 64)
        self.up3 = model_utils.segnetUp3(64, 64)
        self.up2 = model_utils.segnetUp2(64, 64)
        self.up1 = model_utils.segnetUp2(64, n_classes)

    def forward(self, inputs):

        down1, indices_1, unpool_shape1 = self.down1(inputs)
        down2, indices_2, unpool_shape2 = self.down2(down1)
        down3, indices_3, unpool_shape3 = self.down3(down2)
        down4, indices_4, unpool_shape4 = self.down4(down3)
        down5, indices_5, unpool_shape5 = self.down5(down4)

        up5 = self.up5(down5, indices_5, unpool_shape5)
        up4 = self.up4(up5, indices_4, unpool_shape4)
        up3 = self.up3(up4, indices_3, unpool_shape3)
        up2 = self.up2(up3, indices_2, unpool_shape2)
        up1 = self.up1(up2, indices_1, unpool_shape1)

        return up1

    def init_vgg16_params(self, vgg16):
        blocks = [self.down1,
                  self.down2,
                  self.down3,
                  self.down4,
                  self.down5]

        ranges = [[0, 4], [5, 9], [10, 16], [17, 23], [24, 29]]
        features = list(vgg16.features.children())

        vgg_layers = []
        for _layer in features:
            if isinstance(_layer, nn.Conv2d):
                vgg_layers.append(_layer)

        merged_layers = []
        for idx, conv_block in enumerate(blocks):
            if idx < 2:
                units = [conv_block.conv1.cbr_unit,
                         conv_block.conv2.cbr_unit]
            else:
                units = [conv_block.conv1.cbr_unit,
                         conv_block.conv2.cbr_unit,
                         conv_block.conv3.cbr_unit]
            for _unit in units:
                for _layer in _unit:
                    if isinstance(_layer, nn.Conv2d):
                        merged_layers.append(_layer)

        assert len(vgg_layers) == len(merged_layers)

        for l1, l2 in zip(vgg_layers, merged_layers):
            if isinstance(l1, nn.Conv2d) and isinstance(l2, nn.Conv2d):
                assert l1.weight.size() == l2.weight.size()
                assert l1.bias.size() == l2.bias.size()
                l2.weight.data = l1.weight.data
                l2.bias.data = l1.bias.data


class MySegnet2(nn.Module):
    def __init__(self, n_classes=11, in_channels=3, is_unpooling=True):
        super(MySegnet2, self).__init__()

        self.in_channels = in_channels
        self.is_unpooling = is_unpooling

        self.down1 = model_utils.MysegnetDown(self.in_channels, 32)
        self.down2 = model_utils.MysegnetDown(35, 64)
        self.down3 = model_utils.MysegnetDown(99, 64)
        self.down4 = model_utils.MysegnetDown(163, 64)
        self.down5 = model_utils.MysegnetDown(227, 64)
        self.down6 = model_utils.MysegnetDown(291, 64)
        self.down7 = model_utils.MysegnetDown(355, 64)

        self.up7 = model_utils.segnetUp1(419, 355)
        self.up6 = model_utils.segnetUp1(355, 291)
        self.up5 = model_utils.segnetUp1(291, 227)
        self.up4 = model_utils.segnetUp1(227, 163)
        self.up3 = model_utils.segnetUp1(163, 99)
        self.up2 = model_utils.segnetUp1(99, 35)
        self.up1 = model_utils.segnetUp1(35, n_classes)

    def forward(self, inputs):
        down1, indices_1, unpool_shape1 = self.down1(inputs)
        down2, indices_2, unpool_shape2 = self.down2(down1)
        down3, indices_3, unpool_shape3 = self.down3(down2)
        down4, indices_4, unpool_shape4 = self.down4(down3)
        down5, indices_5, unpool_shape5 = self.down5(down4)
        down6, indices_6, unpool_shape6 = self.down6(down5)
        down7, indices_7, unpool_shape7 = self.down7(down6)

        up7 = self.up7(down7, indices_7, unpool_shape7)
        up6 = self.up6(up7, indices_6, unpool_shape6)
        up5 = self.up5(up6, indices_5, unpool_shape5)
        up4 = self.up4(up5, indices_4, unpool_shape4)
        up3 = self.up3(up4, indices_3, unpool_shape3)
        up2 = self.up2(up3, indices_2, unpool_shape2)
        up1 = self.up1(up2, indices_1, unpool_shape1)

        return up1


class DeepUnet(nn.Module):
    def __init__(self, n_classes=11, in_channels=3, is_unpooling=True):
        super(DeepUnet, self).__init__()

        self.in_channels = in_channels
        self.is_unpooling = is_unpooling
        self.pool = model_utils.ThePool()

        self.prepa = model_utils.simpleconv(self.in_channels, 32)
        self.down1 = model_utils.DeepUnetConv2(32, 32)

        self.down2 = model_utils.DeepUnetConv2(32, 32)

        self.down3 = model_utils.DeepUnetConv2(32, 32)
        self.down4 = model_utils.DeepUnetConv2(32, 32)
        self.down5 = model_utils.DeepUnetConv2(32, 32)
        self.down6 = model_utils.DeepUnetConv2(32, 32)
        self.down7 = model_utils.DeepUnetConv2(32, 32)

        self.up7 = model_utils.DeepUnetUp1In(32, 32)
        self.up6 = model_utils.DeepUnetUp(32, 32)
        self.up5 = model_utils.DeepUnetUp(32, 32)
        self.up4 = model_utils.DeepUnetUp(32, 32)
        self.up3 = model_utils.DeepUnetUp(32, 32)
        self.up2 = model_utils.DeepUnetUp(32, 32)
        self.up1 = model_utils.DeepUnetlast(32, n_classes)

    def forward(self, inputs):
        inputs = self.prepa(inputs)

        down1 = self.down1(inputs)

        pool = self.pool(down1)
        down2 = self.down2(pool)
        pool = self.pool(down2)
        down3 = self.down3(pool)
        pool = self.pool(down3)
        down4 = self.down4(pool)
        pool = self.pool(down4)
        down5 = self.down5(pool)
        pool = self.pool(down5)
        down6 = self.down6(pool)
        pool = self.pool(down6)
        down7 = self.down7(pool)
        pool = self.pool(down7)

        up7 = self.up7(pool)
        up6 = self.up6(up7, down6)

        up5 = self.up5(up6, down5)
        up4 = self.up4(up5, down4)
        up3 = self.up3(up4, down3)
        up2 = self.up2(up3, down2)
        up1 = self.up1(up2, down1)

        return up1

