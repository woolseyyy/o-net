import cv2
import cv
import numpy as np
import os
import shutil
import tifffile as tiff
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--data_path", required=True, help="directory containing data")
parser.add_argument("--label_path", required=True, help="directory containing label")
parser.add_argument("--output_path", default="./", help="the table record relationship between data and label")
parser.add_argument("--data_processed_path", default="./temp/train/data/", help="directory containing processed data")
parser.add_argument("--label_processed_path", default="./temp/train/label/", help="directory containing processed label")

a = parser.parse_args()

if not a.data_path.endswith('/'):
    a.data_path = a.data_path + '/'
if not a.label_path.endswith('/'):
    a.label_path = a.label_path + '/'
if not a.output_path.endswith('/'):
    a.output_path = a.output_path + '/'


def data_pre(in_dir, out_dir):
    count = 0

    # clean the path to store the prepared data
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.rmtree(out_dir)
    os.mkdir(out_dir)

    pathDir = os.listdir(in_dir)
    for data in pathDir:

        path = in_dir + "/" + data
        data_name = data.split(".")
        file_type = ""

        # get data_n and read the images in proper way
        if len(data_name) > 1 and data_name[len(data_name) - 1] == "tif":
            data_n = data_name[0]
            file_type = "tif"
        elif len(data_name) > 1 and data_name[len(data_name) - 1] == "png":
            data_n = data_name[0]
            file_type = "png"

        # read the image
        if file_type == "tif":  # tifffile will read image as (chanel x width x height)
            image = tiff.imread(path)
            image_temp = np.zeros((image.shape[1], image.shape[2], image.shape[0]), np.int32)
            for i in [0, 1, 2]:
                image_temp[:, :, 2 - i] = np.int32(image[i, :, :])
            image = image_temp
        elif file_type == "png":
            image = cv2.imread(path)
        else:
            print("skip a file of invalid type: " + path)
            continue

        # cropped to 256x256 with 3 padding and store
        # add padding to the whole images
        image_temp = np.zeros((image.shape[0] + 6, image.shape[1] + 6, image.shape[2]), np.int32)
        image_temp[3:3 + image.shape[0], 3:3 + image.shape[1], :] = image[:, :, :]
        image = image_temp
        # crop
        x_max, y_max = np.int32([np.ceil(image.shape[0] / 250), np.ceil(image.shape[1] / 250)])
        for i in xrange(0, x_max):
            for j in xrange(0, y_max):
                xlen, ylen = [i * 250, j * 250]

                xb, yb = [xlen, ylen]
                if xlen + 256 <= image.shape[0]:
                    xe = xlen + 256
                else:
                    xe = image.shape[0]
                if ylen + 256 <= image.shape[1]:
                    ye = ylen + 256
                else:
                    ye = image.shape[1]

                crop_img = np.zeros((256, 256, 3), np.int32)
                crop_img[0:xe - xb, 0:ye - yb, :] = image[xb:xe, yb:ye, :]

                # normalize
                image_t = np.zeros(crop_img.shape, np.float32)
                for x in [0, 1, 2]:
                    max = crop_img[:, :, x].max()
                    min = crop_img[crop_img[:, :, x] > 0, x].min()
                    image_t[:, :, x] = (crop_img[:, :, x] - min) * 1.0 / (max - min) * 255
                    image_t[image_t[:, :, x] < 0, x] = 0
                crop_img = np.uint8(image_t)

                cv2.imwrite(out_dir + data_n + "_" + str(i) + "_" + str(j) + ".png", crop_img,
                            [int(cv2.IMWRITE_PNG_COMPRESSION), 0])

        count += 1
        print("data pre processing and cropping...%d/%d" % (count, len(pathDir)))

    print(">>> data pre processing and cropping done! <<<")


def label_crop(in_dir, out_dir):
    count = 0

    # clean the path to store the prepared data
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.rmtree(out_dir)
    os.mkdir(out_dir)

    pathDir = os.listdir(in_dir)
    for data in pathDir:

        path = in_dir + "/" + data
        data_name = data.split(".")
        file_type = ""

        # get data_n and read the images in proper way
        if len(data_name) > 1 and data_name[len(data_name) - 1] == "tif":
            data_n = data_name[0]
            file_type = "tif"
        elif len(data_name) > 1 and data_name[len(data_name) - 1] == "png":
            data_n = data_name[0]
            file_type = "png"

        # read the image
        if file_type == "tif":  # tifffile will read image as (chanel x width x height)
            image = tiff.imread(path)
        elif file_type == "png":
            image = cv2.imread(path)
        else:
            print("skip a file of invalid type: " + path)
            continue

        image_t = np.zeros(image.shape, np.uint8)
        for i in range(0, 3):
            image_t[:, :, 2 - i] = image[:, :, i]

        image_24 = np.uint8(image_t)

        # cropped to 256x256 with 3 padding and store
        # add padding to the whole images
        image_24_temp = np.zeros((image_24.shape[0] + 6, image_24.shape[1] + 6, image_24.shape[2]), np.uint8)
        image_24_temp[3:3 + image_24.shape[0], 3:3 + image_24.shape[1], :] = image_24[:, :, :]
        image_24 = image_24_temp
        # crop
        x_max, y_max = np.int32([np.ceil(image_24.shape[0] / 250), np.ceil(image_24.shape[1] / 250)])
        for i in xrange(0, x_max):
            for j in xrange(0, y_max):
                xlen, ylen = [i * 250, j * 250]

                xb, yb = [xlen, ylen]
                if xlen + 256 <= image_24.shape[0]:
                    xe = xlen + 256
                else:
                    xe = image_24.shape[0]
                if ylen + 256 <= image_24.shape[1]:
                    ye = ylen + 256
                else:
                    ye = image_24.shape[1]

                crop_img = np.zeros((256, 256, 3), np.uint8)
                crop_img[0:xe - xb, 0:ye - yb, :] = image_24[xb:xe, yb:ye, :]

                cv2.imwrite(out_dir + data_n + "_" + str(i) + "_" + str(j) + ".png", crop_img,
                            [int(cv2.IMWRITE_PNG_COMPRESSION), 0])

        count += 1
        print("label cropping...%d/%d" % (count, len(pathDir)))

    print(">>> label cropping done! <<<")


def txt_produce(data_path, label_path, output_path):

    data_path = os.path.realpath(data_path)
    if not data_path.endswith('/'):
        data_path = data_path + '/'

    label_path = os.path.realpath(label_path)
    if not label_path.endswith('/'):
        label_path = label_path + '/'

    file_list = os.listdir(data_path)

    # complete detect.txt
    print("conduct segmentation part by part...prepare detect.txt...")
    with open(output_path + 'train.txt', 'w') as f:
        for file in file_list:
            f.write(data_path + file + '\t' + label_path + file + '\n')


data_pre(a.data_path, a.data_processed_path)
label_crop(a.label_path, a.label_processed_path)
txt_produce(a.data_processed_path, a.label_processed_path, a.output_path)
