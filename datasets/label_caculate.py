import cv2
import cv 
import numpy as np
import os
import tifffile as tiff

root="/home/haoqian/Projects/O-Net/datasets/dstl/data_pair/label"
count = 0
classes = np.zeros(11)
pathDir =  os.listdir(root)
for data in pathDir:
    path = root + "/" + data
    data_name = data.split(".")
    if len(data_name) > 1 and data_name[len(data_name)-1] == "png":
        data_n = data_name[0]

    image =  cv2.imread(path)

    for i in xrange(0,11):
        classes[i] += np.sum(image[:,:,0]==i)

    count = count + 1
    print(str(count)+"/"+str(len(pathDir)))

print('-------------------------------------------ok-----------------------------------------------------')
print("numbers:")
print(classes)

per = classes / np.sum(classes)
print("percentage:")
print(per)

weight = np.median(per) / per
print("weight:")
print(weight)

weight = ( np.max(per) + np.min(per) ) / per
weight = np.log(weight)
print("positive log weight:")
print(weight)
        

