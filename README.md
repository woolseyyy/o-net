#O-Net
Using cycle gan to do satellite image segmentation with semi-supervised learning.

![method](./readme/method.jpg)

## Prerequisites

+ Python 2.7
+ PyTorch
+ Numpy/Scipy/Pandas
+ Progressbar
+ OpenCV
+ logger.py which is in the projects

## Usage
```
python ./onet/segmentation.py <args>
```

Please see the optional args using:
```
python ./onet/segmentation.py --help
```

To use Tensorboard, use pip to install the newest version of tensorflow, and put logger.py in the directory of segmentation.py.


example:
```
CUDA_VISIBLE_DEVICES=0 python ./onet/segmentation.py \
  --data_pair ./datasets/dstl/data_pair.txt \
  --data_unpair ./datasets/dstl/data_unpair.txt \
  --test_pair ./datasets/dstl/test_pair.txt \
  --model_path ./probout/models0_2/ \
  --result_path ./probout/results0_2/ \
  --batch_size 4 \
  --class_weight 0.1417,0.6823,1.8418,0.9051,0.3826,0.2595,0.1826,4.5640,6.2478,9.6446,7.3614 \
  --epoch_size_list_A 0,5,5 \
  --epoch_size_list_B 10  \
  --period_size 4 \
  --learning_all_rate 0.000000002  \
  --learning_seg_rate 0.0000001  \
  --segnet_path /home/haoqian/Projects/O-Net/probout/models0/segnet-0-StepA-70000.pkl \
  --generator_path /home/haoqian/Projects/O-Net/probout/models3/generator-2-StepA-12000.pkl \
  --discriminator_path /home/haoqian/Projects/O-Net/probout/models3/discriminator-2-StepA-12000.pkl \
  --gan_curriculum 0   \
  2>&1 | tee ./probout/out0_2.log
```

## Experiment

We experiment on DSTL and ISPRS datasets. An improvment of accuracy is shown on segnet on dstl but failed on ISPRS. And the improvement is not very stable, the accuracy will go down with experiment going on.

Lacking middle supervised information make our method seem failed. It is possible that the semi-supervised learning process makes our segmentation network trained bad.

![experiment](./readme/experiment.png)